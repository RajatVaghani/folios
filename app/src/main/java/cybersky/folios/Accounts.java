package cybersky.folios;

/**
 * Created by Rajat on 9/20/2015.
 */
public class Accounts {

    int id;
    int book_id;
    String type;
    String tags;
    double amount;
    String details;
    String extra;
    int day;
    int month;
    int year;
    String time;

    public Accounts(){

    }

    public Accounts(int id, int book_id, String type, String tags, double amount, String details, String extra, int day, int month, int year, String time) {
        this.id = id;
        this.book_id = book_id;
        this.type = type;
        this.tags = tags;
        this.amount = amount;
        this.details = details;
        this.extra = extra;
        this.day = day;
        this.month = month;
        this.year = year;
        this.time = time;
    }

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
