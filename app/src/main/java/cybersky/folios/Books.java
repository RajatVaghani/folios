package cybersky.folios;

/**
 * Created by Rajat on 9/20/2015.
 */
public class Books {

    int id;
    String name;
    String last_updated;
    double balance;

    public  Books(){

    }

    public Books(int id, String name, String last_updated, double balance) {
        this.id = id;
        this.name = name;
        this.last_updated = last_updated;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
