package cybersky.folios;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.takwolf.android.lock9.Lock9View;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import cybersky.folios.helper.DatabaseHelper;

public class InputPattern extends ActionBarActivity {

    int count=0,iflocked;
    TextView tries,message;
    String pattern;
    TinyDB tinyDB;
    String oldp;
    User user;
    DatabaseHelper databaseHelper;
    ImageView forgotPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_input_pattern);

        tries = (TextView)findViewById(R.id.tries);
        message = (TextView)findViewById(R.id.message);
        forgotPass = (ImageView)findViewById(R.id.forgotPass);

        tinyDB = new TinyDB(getApplicationContext());
        databaseHelper = new DatabaseHelper(getApplicationContext());
        user = databaseHelper.getUser(1);

        oldp = tinyDB.getString("lock");

        tries.setText("");

        message.setText("ENTER PATTERN");

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    fetchPattern fetch= new fetchPattern();
                    fetch.execute();
                }catch(Exception e){

                }
            }
        });

        final Lock9View lock9View = (Lock9View) findViewById(R.id.lock_9_view);
        lock9View.setCallBack(new Lock9View.CallBack() {

            @Override
            public void onFinish(String password) {

                        if(password.equalsIgnoreCase(oldp)){
                            Intent intent = new Intent(InputPattern.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            message.setText("TRY AGAIN!");
                        }

            }

        });

    }

    class fetchPattern extends AsyncTask<Void,Void,Void> {

        ProgressDialog asyncDialog = new ProgressDialog(InputPattern.this);

        @Override
        protected void onPreExecute() {
            asyncDialog.setMessage("Fetching Pattern ...");
            asyncDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            asyncDialog.dismiss();
            Toast.makeText(getApplicationContext(),"Please check your mail for the forgotten pattern.", Toast.LENGTH_SHORT).show();

            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... params) {


            String mess = "Password reset for "+user.getEmail();

            // Create data variable for sent values to server
            String data="";
            try {
                data = URLEncoder.encode("message", "UTF-8")
                        + "=" + URLEncoder.encode(mess, "UTF-8");
            }catch (Exception e){

            }



            String text = "";
            BufferedReader reader=null;

            // Send data
            try
            {

                // Defined URL  where to send data
                URL url = new URL("http://rvcv.in/folios/send_mail.php");

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "\n");
                }


                text = sb.toString();
            }
            catch(Exception ex)
            {

            }
            finally
            {
                try
                {

                    reader.close();
                }

                catch(Exception ex) {}
            }



            return null;
        }
    }

}
