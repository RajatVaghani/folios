package cybersky.folios;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cocosw.bottomsheet.BottomSheet;





import com.squareup.picasso.Picasso;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cybersky.folios.adapters.MainListAdapter;
import cybersky.folios.helper.DatabaseHelper;
import cybersky.folios.helper.IabHelper;
import cybersky.folios.helper.IabResult;
import cybersky.folios.helper.Inventory;
import cybersky.folios.helper.Purchase;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    User user;
    CircleImageView avatar;
    TextView title, subtitle;
    FloatingActionButton fab;
    String currency;
    private static final int PERMISSION_REQUEST_CODE = 1;

    TinyDB tinyDB;

    //----------------- MAIN CONTENT -----------------
    LinearLayout parentLayout;
    TextView msg;
    ListView list;

    //INAPP BILLING
    String tt;
    IabHelper mHelper;
    static final String ITEM_SKU_HALF_YEAR = "half_year";
    static final String ITEM_SKU_LIFETIME = "lifetime";
    static final String ITEM_SKU_MONTHLY = "one_month";
    static final String ITEM_SKU_YEARLY = "yearly";
    static String ITEM_SKU = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);
        toolbar.setTitle("Folios");
        toolbar.inflateMenu(R.menu.menu_main);
        setSupportActionBar(toolbar);

        tinyDB = new TinyDB(getApplicationContext());

        currency = tinyDB.getString("currency");



        if(checkPermission()) {

        }else{
            requestPermission();
        }

        fab = (FloatingActionButton)findViewById(R.id.fab);

        msg = (TextView)findViewById(R.id.msg);
        list = (ListView)findViewById(R.id.list);
        parentLayout = (LinearLayout)findViewById(R.id.root);

        list.setVisibility(View.GONE);

        databaseHelper = new DatabaseHelper(getApplicationContext());
        user = databaseHelper.getUser(1);

        avatar = (CircleImageView)findViewById(R.id.cat_avatar);
        title = (TextView)findViewById(R.id.cat_title);
        subtitle = (TextView)findViewById(R.id.subtitle);

        title.setText(user.getName());
        subtitle.setText("Welcome!");

        Picasso.with(getApplicationContext()).load(user.getImage()).placeholder(R.drawable.user).error(R.drawable.user).into(avatar);

        String base64EncodedPublicKey =
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqY0b5oNMey58PytiHwt2PmPa4oUlCslzWZ+S6cQ4km6DcpwMoFUpCZHRg+zs+8w0O683eieck5csmndWX+qNF3GPO5UKYqWlMXlHEECfnJTIr5Zj5wlPFxyhGIBss30ZorXdrNjIxr5gLgGUuq0xGwQEaphW6SWDiQbUb0kfASMT860Yw955qJU6v1eY5Aw6pEqUciLPPbqmeU8LbAs3O5QeXMTpM4jWmv16urAkNaoU1Su3p/o281KUXmb4KiYFalHM7gHt2N/srtN+BxlHRvO0QIeYTLtrMsd5+QYcN88GcTnrL3r88djk4R2kleBm91kvC8MYw/yUpsgLnqsZwwIDAQAB";

        mHelper = new IabHelper(MainActivity.this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d("TAG", "In-app Billing setup failed: " + result);
                } else {
                    Log.d("TAG", "In-app Billing is set up OK");
                }
            }
        });

        checkEmptyBooks();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (databaseHelper.getBooksCount() == 0) {
                    newAccountDialog();
                } else {
                    newTransactionDialog();
                }
            }
        });







    }


    void newTransactionDialog(){

        final List<Books> allbooks;
        allbooks = databaseHelper.getAllBooks();

        BottomSheet sheet;
        Menu menus;


        sheet = new BottomSheet.Builder(this).title("Select a Folio").listener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


               // Log.e("FOUND ID", ""+idd);
                if(which==-1111) {
                    Intent i = new Intent(getApplicationContext(), TransferAmount.class);
                    startActivity(i);
                }else {
                    int idd = allbooks.get(which).getId();
                    Intent i = new Intent(getApplicationContext(), NewTransaction.class);
                    i.putExtra("id", idd);
                    i.putExtra("edit", 0);
                    startActivity(i);
                }
                dialog.dismiss();
            }
        }).sheet(R.menu.emptysheet).build();

        sheet.setTitle("Select Folio:");

        menus = sheet.getMenu();
        menus.add(-1111,-1111,0,"TRANSFER BETWEEN FOLIOS");
        for(int i=0;i<allbooks.size();i++){
            menus.add(i,i,i+1,allbooks.get(i).getName().toUpperCase(Locale.ENGLISH));
        }



        sheet.show();
        sheet.invalidate();
    }


    void checkEmptyBooks(){

        if(databaseHelper.getBooksCount()==0){
            msg.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
        }else{
            msg.setVisibility(View.GONE);

            List<Books> books;
            books = databaseHelper.getAllBooks();
            Log.e("", ""+books.get(0).getName());
            list.setAdapter(new MainListAdapter(MainActivity.this, books));
            list.setVisibility(View.VISIBLE);

        }

    }


    void newAccountDialog(){



        MaterialDialog dialog = new MaterialDialog.Builder(MainActivity.this).customView(R.layout.new_book, true)
                .positiveText("CREATE")
                .negativeText("CANCEL")
                .title("Create new Folio")
                .cancelable(false)
                .autoDismiss(false)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        View view = dialog.getCustomView();
                        final EditText name = (EditText) view.findViewById(R.id.bookname);
                        final EditText bala = (EditText) view.findViewById(R.id.bookbal);
                        String n = name.getText().toString();
                        Double bal;
                        if (n.length() < 4) {
                            Snackbar
                                    .make(parentLayout, "Name should be at least 4 characters.", Snackbar.LENGTH_LONG)
                                    .show();
                        } else if (bala.getText().toString().length() == 0) {
                            Snackbar
                                    .make(parentLayout, "Please enter an Opening Balance.", Snackbar.LENGTH_LONG)
                                    .show();
                        } else {
                            String t = bala.getText().toString();
                            t = t.substring(currency.length());
                            bal = Double.parseDouble(t);


                            Time time = new Time();
                            time.setToNow();
                            t = String.valueOf(time);
                            String yr = "" + time.year;
                            String month = "" + (time.month + 1);
                            String date = "" + time.monthDay;

                            Books book = new Books();
                            book.setName(n);
                            book.setBalance(bal);
                            book.setLast_updated(date + "/" + month + "/" + yr);

                            databaseHelper.createBooks(book, databaseHelper.getBooksCount() + 1);

                            List<Books> books = new ArrayList<Books>();
                            books.add(book);

                            list.setAdapter(new MainListAdapter(MainActivity.this, books));
                            Snackbar
                                    .make(parentLayout, "New Folio created successfully!", Snackbar.LENGTH_LONG)
                                    .show();

                            checkEmptyBooks();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                    }
                })
                .build();






        final View view = dialog.getCustomView();

        final EditText et = (EditText)view.findViewById(R.id.bookbal);

        //et.setHint("Enter Amount ("+currency+")");
        et.setRawInputType(Configuration.KEYBOARD_12KEY);
        et.addTextChangedListener(new TextWatcher() {
            DecimalFormat dec = new DecimalFormat("0.00");
            String current = "";

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (!s.toString().equals(current)) {
                    et.removeTextChangedListener(this);
                    String replaceable = String.format("[%s,.\\s]", currency);


                    String str = s.toString().replaceAll( "[^\\d]", "" );
                    Log.e("FOUND TEXT IS ", ""+str);
                    double s1 = Double.parseDouble(str);

                    String cleanString = s.toString().replaceAll(replaceable, "");

                    if(cleanString.contains(","))
                        cleanString = cleanString.replaceAll(",",".");

                    double parsed = Double.parseDouble(cleanString);
                    // String formatted = dec.format(parsed);


                    double percen = s1 / 100;
                    current = currency + dec.format(percen);

                    et.setText(current);
                    et.setSelection(current.length());

                    et.addTextChangedListener(this);
                }


            }
        });



        dialog.show();





    }


    public void importExport(){

        // ---------------------------------------------
        // CHANGE FOR PREMIUM USERS
        // 0 = Not Premium
        // 1 = Premium
        // ---------------------------------------------
        if(tinyDB.getInt("inapp",0) == 1 ) {
            new MaterialDialog.Builder(MainActivity.this)
                    .title("Choose Option")
                    .content("You could export the current copy or import an old one.")
                    .positiveText("Restore")
                    .titleColor(Color.parseColor("#333333"))
                    .contentColor(Color.parseColor("#474747"))
                    .negativeText("Backup")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            try {
                                File folder = new File(Environment.getExternalStorageDirectory() + "/Folios");
                                if (!folder.exists()) {
                                    folder.mkdirs();
                                }

                                File sd = Environment.getExternalStorageDirectory();
                                File data = Environment.getDataDirectory();

                                if (sd.canWrite()) {
                                    String currentDBPath = "//data//" + "cybersky.folios"
                                            + "//databases//" + "FolioDB";
                                    String backupDBPath = "//Folios/backup.db";
                                    File backupDB = new File(data, currentDBPath);
                                    File currentDB = new File(sd, backupDBPath);

                                    FileChannel src = new FileInputStream(currentDB).getChannel();
                                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                                    dst.transferFrom(src, 0, src.size());
                                    src.close();
                                    dst.close();
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                    finish();


                                }
                            } catch (FileNotFoundException e) {
                                Snackbar.make(parentLayout, "Backup File not found at Folios/backup.db directory",
                                        Snackbar.LENGTH_SHORT).show();
                            } catch (Exception e) {

                                Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG)
                                        .show();

                            }
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            try {
                                File folder = new File(Environment.getExternalStorageDirectory() + "/Folios");
                                if (!folder.exists()) {
                                    folder.mkdirs();
                                }
                                File sd = Environment.getExternalStorageDirectory();
                                File data = Environment.getDataDirectory();

                                if (sd.canWrite()) {
                                    String currentDBPath = "//data//" + "cybersky.folios"
                                            + "//databases//" + "FolioDB";
                                    String backupDBPath = "/Folios/backup.db";
                                    File currentDB = new File(data, currentDBPath);
                                    File backupDB = new File(sd, backupDBPath);

                                    FileChannel src = new FileInputStream(currentDB).getChannel();
                                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                                    dst.transferFrom(src, 0, src.size());
                                    src.close();
                                    dst.close();
                                    Snackbar.make(parentLayout, "Backup created at " + backupDBPath,
                                            Snackbar.LENGTH_SHORT).show();

                                }
                            } catch (Exception e) {

                                Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG)
                                        .show();

                            }

                        }
                    })
                    .show();


        }else{
            User user = databaseHelper.getUser(1);


            final String tok = user.getName() + "_" + user.getEmail();

            String ops[] = {"Monthly Subscription", "Half-Yearly Subscription", "Yearly Subscription", "Lifetime Subscription"};

            new MaterialDialog.Builder(MainActivity.this)
                    .title("This is a premium feature!")
                    .items(ops)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                            if (which == 0) {
                                mHelper.launchPurchaseFlow(MainActivity.this, ITEM_SKU_MONTHLY, 10001,
                                        mPurchaseFinishedListener, tok);
                                ITEM_SKU = ITEM_SKU_MONTHLY;
                            } else if (which == 1) {
                                mHelper.launchPurchaseFlow(MainActivity.this, ITEM_SKU_HALF_YEAR, 10001,
                                        mPurchaseFinishedListener, tok);
                                ITEM_SKU = ITEM_SKU_HALF_YEAR;
                            } else if (which == 2) {
                                mHelper.launchPurchaseFlow(MainActivity.this, ITEM_SKU_YEARLY, 10001,
                                        mPurchaseFinishedListener, tok);
                                ITEM_SKU = ITEM_SKU_YEARLY;
                            } else if (which == 3) {
                                mHelper.launchPurchaseFlow(MainActivity.this, ITEM_SKU_LIFETIME, 10001,
                                        mPurchaseFinishedListener, tok);
                                ITEM_SKU = ITEM_SKU_LIFETIME;
                            }

                        }
                    })
                    .show();
        }
    }

    void generateFiles(){
        new MaterialDialog.Builder(MainActivity.this)
                .title("Choose Option")
                .content("Select a preferred format for export")
                .positiveText("CSV")
                .titleColor(Color.parseColor("#333333"))
                .contentColor(Color.parseColor("#474747"))
                .neutralText("CANCEL")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        ExportDatabaseCSVTask exportDatabaseCSVTask = new ExportDatabaseCSVTask();
                        exportDatabaseCSVTask.execute();
                    }

                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        dialog.dismiss();

                    }
                })
                .show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        // Assumes current activity is the searchable activity
        ComponentName cn = new ComponentName(this, SearchableActivity.class);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(cn));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

                Intent i = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(i);

                return true;

        }else if(id==R.id.action_newbook){

            newAccountDialog();

            return true;
        }else if(id==R.id.action_split) {
            Intent i = new Intent(MainActivity.this, SplitActivity.class);
            startActivity(i);
            return true;

        }else if(id==R.id.action_export){
            importExport();
        }else if(id==R.id.action_generate){
            generateFiles();
        }



        return super.onOptionsItemSelected(item);
    }






    public class ExportDatabaseCSVTask extends AsyncTask<String, Void, Boolean> {
        private final ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Exporting database...");
            this.dialog.show();
        }

        protected Boolean doInBackground(final String... args) {
            File dbFile = getDatabasePath("FolioDB.db");
            System.out.println(dbFile);  // displays the data base path in your logcat
            File exportDir = new File(Environment.getExternalStorageDirectory(), "/Folios");

            if (!exportDir.exists()) { exportDir.mkdirs(); }

            File file = new File(exportDir, "exported.csv");
            try {
                file.createNewFile();
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
                Cursor curCSV = databaseHelper.fetchAllAccounts();
                csvWrite.writeNext(curCSV.getColumnNames());
                while(curCSV.moveToNext()) {
                    String arrStr[] ={curCSV.getString(0),curCSV.getString(1),curCSV.getString(2),curCSV.getString(3),curCSV.getString(4),curCSV.getString(5),curCSV.getString(6),curCSV.getString(7),curCSV.getString(8),curCSV.getString(9),curCSV.getString(10)};
                    // curCSV.getString(3),curCSV.getString(4)};
                    csvWrite.writeNext(arrStr);
                }
                csvWrite.close();
                curCSV.close();
                return true;
            } catch (IOException e) {
                Log.e("MainActivity", e.getMessage(), e);
                return false;
            }
        }

        protected void onPostExecute(final Boolean success) {
            if (this.dialog.isShowing()) { this.dialog.dismiss(); }
            if (success) {
                Snackbar.make(parentLayout, "Export Successful!",
                        Snackbar.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Export failed", Toast.LENGTH_SHORT).show();
            }
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data)
    {
        if (!mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase)
        {
            if (result.isFailure()) {
                // Handle error
                return;
            }
            else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();
                //Toast.makeText(getApplicationContext(),"PURCHASE SUCCESFUL", Toast.LENGTH_SHORT).show();
            }

        }
    };

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // Handle failure
            } else {
                mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                        mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {

                        tt = purchase.getSku();

                        createInApp createInApp =  new createInApp();
                        createInApp.execute();


                    } else {
                        // handle error
                    }
                }
            };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }











    class createInApp extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            asyncDialog.setMessage("Completing Purchase ...");
            asyncDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // Show response on activity
            asyncDialog.dismiss();

            Toast.makeText(getApplicationContext(), "Purchase Successful", Toast.LENGTH_LONG).show();
            tinyDB.putInt("inapp", 1);
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create data variable for sent values to server

            /*String name = databaseHelper.getUser(1).getName();
            String email = databaseHelper.getUser(1).getEmail();

            String data="";

            try {
                data = URLEncoder.encode("email", "UTF-8")
                        + "=" + URLEncoder.encode(email, "UTF-8");

                data += "&" + URLEncoder.encode("type", "UTF-8") + "="
                        + URLEncoder.encode(tt, "UTF-8");

                data += "&" + URLEncoder.encode("name", "UTF-8") + "="
                        + URLEncoder.encode(name, "UTF-8");
                // Get user defined values

            }catch (Exception e){

            }


            String text = "";
            BufferedReader reader=null;

            // Send data
            try
            {

                // Defined URL  where to send data
                URL url = new URL("http://rvcv.in/folios/insert_purchase.php");

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "\n");
                }


                text = sb.toString();
            }
            catch(Exception ex)
            {

            }
            finally
            {
                try
                {

                    reader.close();
                }

                catch(Exception ex) {}
            }

*/
            return null;
        }
    }



    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.GET_ACCOUNTS);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED){
            Log.e("permissions","given");
            return true;

        } else {

            return false;

        }
    }

    private void requestPermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.GET_ACCOUNTS) && ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){


        } else {

            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.GET_ACCOUNTS},PERMISSION_REQUEST_CODE);
            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(getApplicationContext(),"Permission Granted, Please restart the app!",Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(getApplicationContext(), "Permission Denied, Please restart the app!", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }

}
