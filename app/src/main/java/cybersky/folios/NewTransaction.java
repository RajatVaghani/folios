package cybersky.folios;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import cybersky.folios.adapters.Tags;
import cybersky.folios.helper.DatabaseHelper;
import cybersky.folios.helper.TagsCompletionView;
import info.hoang8f.android.segmented.SegmentedGroup;

public class NewTransaction extends AppCompatActivity {

    TextView book;
    Books books;
    DatabaseHelper databaseHelper;
    Accounts newAccount;
    LinearLayout parentLayout;
    EditText amount, description;
    TagsCompletionView tags;
    ArrayAdapter<Tags> adapter;
    Tags[] taglist;
    String currency;
    RadioButton expense,income;
    TinyDB tinyDB;
    SegmentedGroup type;

    Double oldamount;

    int account_id=0;
    Accounts account;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_transaction);

        if(getIntent().getExtras().getInt("edit") != 0){
            account_id = getIntent().getExtras().getInt("edit");
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(account_id==0)
            toolbar.setTitle("New Transaction");
        else
            toolbar.setTitle("Edit Transaction");
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_new_transaction);


        tinyDB = new TinyDB(getApplicationContext());
        currency = tinyDB.getString("currency");

        parentLayout = (LinearLayout)findViewById(R.id.rootlayout);

        type = (SegmentedGroup)findViewById(R.id.segmented2);

        amount= (EditText)findViewById(R.id.amount);
        description = (EditText)findViewById(R.id.particular);
        tags = (TagsCompletionView)findViewById(R.id.tags);

        expense = (RadioButton)findViewById(R.id.button1);
        income = (RadioButton)findViewById(R.id.button2);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        databaseHelper = new DatabaseHelper(getApplicationContext());
        Log.e("count", ""+databaseHelper.getBooksCount());
        book = (TextView)findViewById(R.id.bookName);

        int d = getIntent().getExtras().getInt("id");
        //d=d+1;
        Log.e("id", ""+d);
        books = databaseHelper.getSingleBook(d);

        String alltags[] = databaseHelper.getAllTags();
        taglist = new Tags[alltags.length];
        for(int i =0;i<alltags.length;i++){
            Tags tag= new Tags(alltags[i]);
            taglist[i] = tag;
        }

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, taglist);
        tags.setAdapter(adapter);
        tags.setSplitChar(',');
        tags.allowDuplicates(false);
        tags.performBestGuess(false);
        tags.setTokenLimit(10);

        if(account_id!=0){
            book.setText("Editing in '" + books.getName().toUpperCase(Locale.ENGLISH) + "'");
        }else {
            book.setText("Adding in '" + books.getName().toUpperCase(Locale.ENGLISH) + "'");
        }
        //amount.setHint("Enter Amount ("+currency+")");
        amount.setRawInputType(Configuration.KEYBOARD_12KEY);
        amount.addTextChangedListener(new TextWatcher() {
            DecimalFormat dec = new DecimalFormat("0.00");
            String current="";

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if(!s.toString().equals(current)){
                    amount.removeTextChangedListener(this);
                    String replaceable = String.format("[%s,.\\s]", currency);

                    String cleanString = s.toString().replaceAll(replaceable, "");

                    double parsed = Double.parseDouble(cleanString);


                    String str = s.toString().replaceAll( "[^\\d]", "" );
                    Log.e("FOUND TEXT IS ", ""+str);
                    double s1 = Double.parseDouble(str);
                   // String formatted = dec.format(parsed);


                    double percen = s1/100;
                    current = currency+dec.format(percen);

                    amount.setText(current);
                    amount.setSelection(current.length());

                    amount.addTextChangedListener(this);
                }


            }
        });

        if(account_id !=0){
            Log.e("ACCOUNTID", ""+account_id);

            account = databaseHelper.getSingleAccount(account_id);

            if(account.getType().equalsIgnoreCase("expense"))
                type.check(R.id.button1);
            else
                type.check(R.id.button2);

            amount.setText(currency+(account.getAmount()*10));
            oldamount = account.getAmount()*10;
            description.setText(account.getDetails());

            String tagsarr[] = account.getTags().split(",");
            int c=0;
            for(int i=0;i<tagsarr.length;i++){
                if(tagsarr[i].trim().equalsIgnoreCase("")){

                }else {
                    Tags tag = new Tags(tagsarr[i]);
                    tags.addObject(tag);
                    c++;
                    Log.e("tag", tagsarr[i]);
                }
            }

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_transaction, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {

            Double amo=0.0;
            int m = 0;

            if(amount.getText().toString().length()>0) {
                try{
                    String t = amount.getText().toString();
                    currency = tinyDB.getString("currency");
                    //Toast.makeText(getApplicationContext(), currency, Toast.LENGTH_LONG).show();
                    //t = t.replaceAll(currency, "");
                    t = t.substring(currency.length());
                    amo = Double.parseDouble(t);
                    if (amo != 0.0) {
                        m = 1;

                    } else {
                        m = 0;
                        Snackbar.make(parentLayout, "Please fill in all the fields.", Snackbar.LENGTH_LONG).show();
                    }
                }catch(Exception e){
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                }
            }else if(description.getText().toString().trim().length()==0){
                m=0;
                Snackbar.make(parentLayout, "Please fill in the particulars field.", Snackbar.LENGTH_LONG).show();
            }
            Log.e("SHOULD WORK", ""+m);

            if(m==1){
                newAccount = new Accounts();
                newAccount.setAmount(amo);
                newAccount.setDetails(description.getText().toString().trim());
                newAccount.setExtra("");

                List<Tags> addedt = tags.getObjects();
                String addedtags = "";
                for(int i=0;i<addedt.size();i++) {
                    addedtags = addedtags + "," + addedt.get(i);
                }

                newAccount.setTags(addedtags);
                newAccount.setBook_id(books.getId());
                String ty;
                int which = type.getCheckedRadioButtonId();
                if(which==R.id.button1)
                    ty = "EXPENSE";
                else
                    ty = "INCOME";
                newAccount.setType(ty);

                String month,date,tim,yr;
                if(account_id==0) {
                    Time time = new Time(Time.getCurrentTimezone());
                    time.setToNow();
                    String t = String.valueOf(time);
                    yr = "" + time.year;
                    month = "" + (time.month);
                    date = "" + time.monthDay;
                    tim = time.format("%k:%M:%S");
                }else{
                    yr = "" + account.getYear();
                    month = "" + account.getMonth();
                    date = "" + account.getDay();
                    tim = account.getTime();
                }

                newAccount.setDay(Integer.parseInt(date));
                newAccount.setMonth(Integer.parseInt(month));
                newAccount.setYear(Integer.parseInt(yr));
                newAccount.setTime(tim);

                if(account_id==0) {

                    if (ty.equalsIgnoreCase("EXPENSE")) {
                        double bal = books.getBalance() - amo;
                        books.setBalance(bal);
                    } else {
                        double bal = books.getBalance() + amo;
                        books.setBalance(bal);
                    }
                    books.setLast_updated(date + "/" + month + "/" + yr);
                    databaseHelper.updateBook(books);
                    databaseHelper.createAccounts(newAccount, books.getId());

                }else{

                    double old = account.getAmount();
                    double news = newAccount.getAmount();

                    if(news>old){
                        double diff = news-old;
                        if(ty.equalsIgnoreCase("EXPENSE")){
                            double bal = books.getBalance() - (diff);
                            books.setBalance(bal);
                        }else{
                            double bal = books.getBalance() + (diff);
                            books.setBalance(bal);
                        }
                    }else if(news<old){
                        double diff = old-news;
                        if(ty.equalsIgnoreCase("EXPENSE")) {
                            double bal = books.getBalance() + (diff);
                            books.setBalance(bal);
                        }else{
                            double bal = books.getBalance() - (diff);
                            books.setBalance(bal);
                        }
                    }else if(news==old){

                        if(!(newAccount.getType().equalsIgnoreCase(account.getType()))){
                            double bal = books.getBalance();
                            if(newAccount.getType().equalsIgnoreCase("EXPENSE")) {
                                bal = bal - news - news;
                            }else{
                                bal = bal + news + news;
                            }
                            books.setBalance(bal);
                        } else{
                            double bal = books.getBalance();
                            books.setBalance(bal);
                        }

                    }

                    newAccount.setId(account.getId());

                        databaseHelper.updateBook(books);
                        databaseHelper.updateAccount(newAccount);



                }


                Intent i = new Intent(NewTransaction.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();



            }


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
