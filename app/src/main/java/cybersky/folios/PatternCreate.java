package cybersky.folios;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.takwolf.android.lock9.Lock9View;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import cybersky.folios.helper.DatabaseHelper;


public class PatternCreate extends ActionBarActivity {

    int count=0,iflocked;
    TextView tries,message;
    String pattern;
    TinyDB tinyDB;
    String oldp;

    User user;
    DatabaseHelper databaseHelper;

    ImageView remove;

    Lock9View lock9View;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_pattern_create);
        tries = (TextView)findViewById(R.id.tries);
        message = (TextView)findViewById(R.id.message);

        remove = (ImageView)findViewById(R.id.removePass);

        databaseHelper = new DatabaseHelper(getApplicationContext());

        tinyDB = new TinyDB(getApplicationContext());

        user = databaseHelper.getUser(1);

        iflocked = tinyDB.getInt("islocked",0);


        if(iflocked==0){
            message.setText("CREATE PATTERN");
            remove.setVisibility(View.INVISIBLE);
        }else{
            message.setText("ENTER PATTERN");
            oldp = tinyDB.getString("lock");
            remove.setVisibility(View.VISIBLE);
        }

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog dialog = new MaterialDialog.Builder(PatternCreate.this)
                        .content("Are you sure you want to remove your pattern?")
                        .positiveText("YES")
                        .negativeText("NO")
                        .title("Pattern Lock")
                        .cancelable(false)
                        .autoDismiss(false)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                tinyDB.putInt("islocked", 0);
                                tinyDB.putString("lock", "");
                                Toast.makeText(getApplicationContext(), "Pattern security removed", Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                dialog.dismiss();
                            }
                        })
                        .build();

                dialog.show();
            }
        });



        lock9View = (Lock9View) findViewById(R.id.lock_9_view);
        lock9View.setCallBack(new Lock9View.CallBack() {

            @Override
            public void onFinish(String password) {
               if(iflocked==0) {
                    count++;
                    if (count == 1) {
                        pattern = password;
                        tries.setText("REPEAT SAME PATTERN");
                    }else if(count==2){
                        if(password.equalsIgnoreCase(pattern)){


                            try {
                                createUser createU = new createUser();
                                createU.execute();
                            }catch (Exception e){

                            }


                        }else{
                            pattern="";
                            count=0;
                            tries.setText("START AGAIN!");
                        }
                    }
                }else{
                   if(oldp.equalsIgnoreCase(password)){
                       iflocked = 0;
                       count = 0;
                       message.setText("SET NEW PATTERN");
                       tries.setText("");
                   }else{
                       message.setText("WRONG PATTERN");
                       tries.setText("TRY AGAIN!");
                   }
               }
            }

        });

    }

    class createUser extends AsyncTask<Void, Void, Void>{

        ProgressDialog asyncDialog = new ProgressDialog(PatternCreate.this);

        @Override
        protected void onPreExecute() {
            asyncDialog.setMessage("Saving");
            asyncDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // Show response on activity
            asyncDialog.dismiss();
            lock9View.setVisibility(View.GONE);
            tries.setText("");
            message.setText("PATTERN SAVED!");
            Toast.makeText(getApplicationContext(), "Pattern security set", Toast.LENGTH_LONG).show();
            tinyDB.putInt("islocked", 1);
            tinyDB.putString("lock", pattern);
            finish();
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... params) {

            String email = user.getEmail();
            String patterns = pattern;
            // Create data variable for sent values to server

            String data="";

            try {
                data = URLEncoder.encode("email", "UTF-8")
                        + "=" + URLEncoder.encode(email, "UTF-8");

                data += "&" + URLEncoder.encode("pattern", "UTF-8") + "="
                        + URLEncoder.encode(patterns, "UTF-8");
                // Get user defined values

            }catch (Exception e){

            }


            String text = "";
            BufferedReader reader=null;

            // Send data
            try
            {

                // Defined URL  where to send data
                URL url = new URL("http://rvcv.in/folios/insert_user.php");

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "\n");
                }


                text = sb.toString();
            }
            catch(Exception ex)
            {

            }
            finally
            {
                try
                {

                    reader.close();
                }

                catch(Exception ex) {}
            }


            return null;
        }
    }


}
