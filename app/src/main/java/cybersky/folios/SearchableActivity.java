package cybersky.folios;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cybersky.folios.adapters.SearchAdapter;
import cybersky.folios.helper.DatabaseHelper;

public class SearchableActivity extends ListActivity {

    private TextView text;

    List<Accounts> accountsList;

    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Search Results");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));

        databaseHelper = new DatabaseHelper(getApplicationContext());


        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            toolbar.setTitle("Search Results for '" + query + "'");
            doMySearch(query);
        }

    }

    void doMySearch(String q){

        accountsList = new ArrayList<>();
        accountsList = databaseHelper.searchAccounts(q);





        SearchAdapter myAdapter = new SearchAdapter(SearchableActivity.this,accountsList );

        // assign the list adapter
        setListAdapter(myAdapter);
    }

    @Override
    protected void onListItemClick(ListView list, View view, int position, long id) {
        super.onListItemClick(list, view, position, id);

        String selectedItem = (String) getListView().getItemAtPosition(position);
        //String selectedItem = (String) getListAdapter().getItem(position);

        text.setText("You clicked " + selectedItem + " at position " + position);
    }

}
