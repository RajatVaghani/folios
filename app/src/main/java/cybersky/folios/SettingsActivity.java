package cybersky.folios;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;

import cybersky.folios.helper.DatabaseHelper;
import cybersky.folios.helper.IabHelper;
import cybersky.folios.helper.IabResult;
import cybersky.folios.helper.Inventory;
import cybersky.folios.helper.Purchase;

public class SettingsActivity extends AppCompatActivity {

    TextView settings_currency;
    LinearLayout change_currency, manage_folios, manage_password, manage_tags;

    String[] currency_names, currency_symbols;
    String[] account_names;

    TinyDB tinyDB;
    LinearLayout root;

    DatabaseHelper databaseHelper;
    List<Books> books;

    //INAPP BILLING
    String tt;
    IabHelper mHelper;
    static final String ITEM_SKU_HALF_YEAR = "half_year";
    static final String ITEM_SKU_LIFETIME = "lifetime";
    static final String ITEM_SKU_MONTHLY = "one_month";
    static final String ITEM_SKU_YEARLY = "yearly";
    static String ITEM_SKU = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        tinyDB = new TinyDB(getApplicationContext());
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);
        toolbar.inflateMenu(R.menu.menu_main);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Settings");

        databaseHelper = new DatabaseHelper(getApplicationContext());

        String cur = tinyDB.getString("currency");
        root = (LinearLayout) findViewById(R.id.root);

        settings_currency = (TextView) findViewById(R.id.settingcurrency);
        change_currency = (LinearLayout) findViewById(R.id.editcurrency);

        manage_tags = (LinearLayout) findViewById(R.id.edithash);

        manage_password = (LinearLayout) findViewById(R.id.editpassword);

        manage_folios = (LinearLayout) findViewById(R.id.editaccounts);


        String base64EncodedPublicKey =
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqY0b5oNMey58PytiHwt2PmPa4oUlCslzWZ+S6cQ4km6DcpwMoFUpCZHRg+zs+8w0O683eieck5csmndWX+qNF3GPO5UKYqWlMXlHEECfnJTIr5Zj5wlPFxyhGIBss30ZorXdrNjIxr5gLgGUuq0xGwQEaphW6SWDiQbUb0kfASMT860Yw955qJU6v1eY5Aw6pEqUciLPPbqmeU8LbAs3O5QeXMTpM4jWmv16urAkNaoU1Su3p/o281KUXmb4KiYFalHM7gHt2N/srtN+BxlHRvO0QIeYTLtrMsd5+QYcN88GcTnrL3r88djk4R2kleBm91kvC8MYw/yUpsgLnqsZwwIDAQAB";

        mHelper = new IabHelper(SettingsActivity.this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d("TAG", "In-app Billing setup failed: " + result);
                } else {
                    Log.d("TAG", "In-app Billing is set up OK");
                }
            }
        });

        settings_currency.setText("Current Currency: " + cur);
        currency_names = getResources().getStringArray(R.array.currency_name);
        currency_symbols = getResources().getStringArray(R.array.currency_symbol);

        int countofbooks = databaseHelper.getBooksCount();

        account_names = new String[countofbooks];

        books = databaseHelper.getAllBooks();
        for (int i = 0; i < countofbooks; i++) {
            account_names[i] = books.get(i).getName();
        }

        change_currency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(SettingsActivity.this)
                        .title("Select Currency to Use")
                        .items(currency_names)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                String cur = currency_symbols[which];
                                settings_currency.setText("Current Currency: " + cur);
                                tinyDB.putString("currency", cur);
                                Intent i = new Intent(SettingsActivity.this, MainActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            }
                        })
                        .show();
            }
        });


        manage_folios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(SettingsActivity.this)
                        .title("Remove Accounts")
                        .items(account_names)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                                List<Accounts> accounts;
                                Books book = books.get(which);
                                accounts = databaseHelper.getAllAccounts(book.getId());
                                for (int i = 0; i < accounts.size(); i++) {
                                    databaseHelper.deleteAccount(accounts.get(i).getId());
                                }

                                databaseHelper.deleteBook(book.getId());

                                Intent i = new Intent(SettingsActivity.this, MainActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            }
                        })
                        .show();
            }
        });


        manage_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // ---------------------------------------------
                // CHANGE FOR PREMIUM USERS
                // 1 = Not Premium
                // 0 = Premium
                // ---------------------------------------------
                if(tinyDB.getInt("inapp",0) == 0 ) {

                    User user = databaseHelper.getUser(1);


                    final String tok = user.getName() + "_" + user.getEmail();

                    String ops[] = {"Monthly Subscription", "Half-Yearly Subscription", "Yearly Subscription", "Lifetime Subscription"};

                    new MaterialDialog.Builder(SettingsActivity.this)
                            .title("This is a premium feature!")
                            .items(ops)
                            .itemsCallback(new MaterialDialog.ListCallback() {
                                @Override
                                public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                                    if (which == 0) {
                                        mHelper.launchPurchaseFlow(SettingsActivity.this, ITEM_SKU_MONTHLY, 10001,
                                                mPurchaseFinishedListener, tok);
                                        ITEM_SKU = ITEM_SKU_MONTHLY;
                                    } else if (which == 1) {
                                        mHelper.launchPurchaseFlow(SettingsActivity.this, ITEM_SKU_HALF_YEAR, 10001,
                                                mPurchaseFinishedListener, tok);
                                        ITEM_SKU = ITEM_SKU_HALF_YEAR;
                                    } else if (which == 2) {
                                        mHelper.launchPurchaseFlow(SettingsActivity.this, ITEM_SKU_YEARLY, 10001,
                                                mPurchaseFinishedListener, tok);
                                        ITEM_SKU = ITEM_SKU_YEARLY;
                                    } else if (which == 3) {
                                        mHelper.launchPurchaseFlow(SettingsActivity.this, ITEM_SKU_LIFETIME, 10001,
                                                mPurchaseFinishedListener, tok);
                                        ITEM_SKU = ITEM_SKU_LIFETIME;
                                    }

                                }
                            })
                            .show();

                }else {


                    if(tinyDB.getInt("islocked",0) == 0){
                        MaterialDialog dialog = new MaterialDialog.Builder(SettingsActivity.this)
                                .content("Add a pattern lock to make sure nobody else can access your Folios. You will have to enter it each time you open the application.")
                                .positiveText("CONTINUE")
                                .negativeText("CANCEL")
                                .title("Pattern Lock")
                                .cancelable(false)
                                .autoDismiss(false)
                                .callback(new MaterialDialog.ButtonCallback() {
                                    @Override
                                    public void onPositive(MaterialDialog dialog) {
                                        Intent in = new Intent(getApplicationContext(), PatternCreate.class);
                                        dialog.dismiss();
                                        startActivity(in);
                                    }

                                    @Override
                                    public void onNegative(MaterialDialog dialog) {
                                        dialog.dismiss();
                                    }
                                })
                                .build();

                        dialog.show();
                    }else{
                        Intent in = new Intent(getApplicationContext(), PatternCreate.class);
                        startActivity(in);
                    }

                }
            }
        });

        manage_tags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(SettingsActivity.this, TagsActivity.class);
                startActivity(in);
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data)
    {
        if (!mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase)
        {
            if (result.isFailure()) {
                // Handle error
                return;
            }
            else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();
                //Toast.makeText(getApplicationContext(),"PURCHASE SUCCESFUL", Toast.LENGTH_SHORT).show();
            }

        }
    };

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // Handle failure
            } else {
                mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                        mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {

                        tt = purchase.getSku();

                        createInApp createInApp =  new createInApp();
                        createInApp.execute();


                    } else {
                        // handle error
                    }
                }
            };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }











    class createInApp extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(SettingsActivity.this);

        @Override
        protected void onPreExecute() {
            asyncDialog.setMessage("Completing Purchase ...");
            asyncDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // Show response on activity
            asyncDialog.dismiss();

            Toast.makeText(getApplicationContext(), "Purchase Successful", Toast.LENGTH_LONG).show();
            tinyDB.putInt("inapp", 1);
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create data variable for sent values to server

            /*String name = databaseHelper.getUser(1).getName();
            String email = databaseHelper.getUser(1).getEmail();

            String data="";

            try {
                data = URLEncoder.encode("email", "UTF-8")
                        + "=" + URLEncoder.encode(email, "UTF-8");

                data += "&" + URLEncoder.encode("type", "UTF-8") + "="
                        + URLEncoder.encode(tt, "UTF-8");

                data += "&" + URLEncoder.encode("name", "UTF-8") + "="
                        + URLEncoder.encode(name, "UTF-8");
                // Get user defined values

            }catch (Exception e){

            }


            String text = "";
            BufferedReader reader=null;

            // Send data
            try
            {

                // Defined URL  where to send data
                URL url = new URL("http://rvcv.in/folios/insert_purchase.php");

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "\n");
                }


                text = sb.toString();
            }
            catch(Exception ex)
            {

            }
            finally
            {
                try
                {

                    reader.close();
                }

                catch(Exception ex) {}
            }
*/

            return null;
        }
    }

}
