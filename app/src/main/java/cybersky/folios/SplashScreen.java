package cybersky.folios;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import cybersky.folios.helper.DatabaseHelper;

/**
 * Created by Rajat on 9/24/2015.
 */
public class SplashScreen extends AppCompatActivity {

    TinyDB tinyDB;
    DatabaseHelper databaseHelper;
    int m,islock;
    private View view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.splash);

        databaseHelper = new DatabaseHelper(getApplicationContext());

        tinyDB = new TinyDB(getApplicationContext());
        m = tinyDB.getInt("logged_in",0);
        islock = tinyDB.getInt("islocked",0);

        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(1100);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Intent intent;
                    if(m==1) {
                        if(islock ==0) {
                            intent = new Intent(SplashScreen.this, MainActivity.class);
                        }else{
                            intent = new Intent(SplashScreen.this, InputPattern.class);
                        }
                    }else{
                        intent = new Intent(SplashScreen.this, LoginActivity.class);
                    }
                    startActivity(intent);
                    finish();
                }
            }
        };
        timerThread.start();

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }







/*

    class fetchInApp extends AsyncTask<Void,Void,Void> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... params) {

            if(m==1) {

                String data="";

                try {
				
                // Create data variable for sent values to server
                data = databaseHelper.getUser(1).getEmail();
               // Log.e("Sending", data);
                    data = URLEncoder.encode("email", "UTF-8")
                            + "=" + URLEncoder.encode(data, "UTF-8");
                } catch (Exception e) {

                }


                String text = "";
                BufferedReader reader = null;

                // Send data
                try {

                    // Defined URL  where to send data
                    URL url = new URL("http://rvcv.in/folios/find_inapp.php");

                    // Send POST data request

                    URLConnection conn = url.openConnection();
                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write(data);
                    wr.flush();

                    // Get the server response

                    reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while ((line = reader.readLine()) != null) {
                        // Append server response in string
                        sb.append(line + "\n");
                    }


                    text = sb.toString();
                    Log.e("text", text);

                    if(text.contains("YES")){
                        tinyDB.putInt("inapp", 1);
                        Log.e("Found it", "found it");
                    }else{
                        tinyDB.putInt("inapp", 0);
                        Log.e("Didnt find", "didnt find");
                    }

                } catch (Exception ex) {

                } finally {
                    try {
                        reader.close();
                    } catch (Exception ex) {

                    }


                }

            }

            return null;
        }
    }


*/

}
