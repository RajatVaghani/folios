package cybersky.folios;

import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Size;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import info.hoang8f.android.segmented.SegmentedGroup;

public class SplitActivity extends AppCompatActivity {

    EditText amount, ppl;
    RadioButton byper,byequ;

    SegmentedGroup type;


    LinearLayout results,conclusion;
    TextView conc_text;


    ArrayList<TextView> tViews = new ArrayList<TextView>();
    ArrayList<EditText> tEdits = new ArrayList<>();

    TinyDB tinyDB;
    String currency;

    int numberofppl, modeofsending;
    double divides[], totalamount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Split Any Transaction");


        tinyDB = new TinyDB(getApplicationContext());
        currency = tinyDB.getString("currency");

        amount = (EditText)findViewById(R.id.amount);
        ppl = (EditText)findViewById(R.id.numberofppl);

        byper = (RadioButton)findViewById(R.id.button1);
        byequ = (RadioButton)findViewById(R.id.button3);
        type = (SegmentedGroup)findViewById(R.id.segmented2);

        results = (LinearLayout)findViewById(R.id.results);
        conclusion = (LinearLayout)findViewById(R.id.conclusion);
        conc_text = (TextView)findViewById(R.id.conc_txt);
        conclusion.setVisibility(View.INVISIBLE);
        results.setVisibility(View.INVISIBLE);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        amount.setRawInputType(Configuration.KEYBOARD_12KEY);
        amount.addTextChangedListener(new TextWatcher() {
            DecimalFormat dec = new DecimalFormat("0.00");
            String current = "";

            @Override
            public void afterTextChanged(Editable arg0) {
                    if(ppl.length()>0 && arg0.toString().length()>0){
                        if(byper.isChecked()){
                            int n = Integer.parseInt(ppl.getText().toString());
                            double am = Double.parseDouble(amount.getText().toString().substring(currency.length()));
                            double arr[] = new double[n];
                            arr = dividePercentages(n,am);
                            totalamount = am;
                            divides = arr;
                            numberofppl = n;
                            results.removeAllViews();
                            tViews.clear();
                            tEdits.clear();
                            showSplit(1,n,arr,am);
                        }else if(byequ.isChecked()){
                            int n = Integer.parseInt(ppl.getText().toString());
                            double am = Double.parseDouble(amount.getText().toString().substring(currency.length()));
                            double arr[] = new double[n];
                            arr = divideEqually(n,am);
                            totalamount = am;
                            divides = arr;
                            numberofppl = n;
                            results.removeAllViews();
                            tViews.clear();
                            tEdits.clear();
                            showSplit(3,n,arr,am);
                        }
                    }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (!s.toString().equals(current)) {
                    amount.removeTextChangedListener(this);
                    String replaceable = String.format("[%s,.\\s]", currency);

                    String cleanString = s.toString().replaceAll(replaceable, "");

                    double parsed = Double.parseDouble(cleanString);
                    // String formatted = dec.format(parsed);


                    double percen = parsed / 100;
                    current = currency + dec.format(percen);

                    amount.setText(current);
                    amount.setSelection(current.length());

                    amount.addTextChangedListener(this);
                }


            }
        });

        ppl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(amount.length()>0 && s.toString().length()>0){
                    if(byper.isChecked()){
                        int n = Integer.parseInt(ppl.getText().toString());
                        double am = Double.parseDouble(amount.getText().toString().substring(currency.length()));
                        double arr[] = new double[n];
                        arr = dividePercentages(n,am);
                        totalamount = am;
                        divides = arr;
                        numberofppl = n;
                        results.removeAllViews();
                        tViews.clear();
                        tEdits.clear();
                        showSplit(1,n,arr,am);
                    }else if(byequ.isChecked()){
                        int n = Integer.parseInt(ppl.getText().toString());
                        double am = Double.parseDouble(amount.getText().toString().substring(currency.length()));
                        double arr[] = new double[n];
                        arr = divideEqually(n,am);
                        totalamount = am;
                        divides = arr;
                        numberofppl = n;
                        results.removeAllViews();
                        tViews.clear();
                        tEdits.clear();
                        showSplit(3,n,arr,am);
                    }
                }
            }
        });

        byper.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked==true && amount.getText().toString().length()>0 && ppl.getText().toString().length()>0){
                    Log.e("----- Dividing equally", "-----");
                    int n = Integer.parseInt(ppl.getText().toString());
                    double am = Double.parseDouble(amount.getText().toString().substring(currency.length()));
                    double arr[] = new double[n];
                    arr = dividePercentages(n,am);
                    totalamount = am;
                    divides = arr;
                    numberofppl = n;
                    results.removeAllViews();
                    tViews.clear();
                    tEdits.clear();
                    showSplit(1,n,arr,am);
                }
            }
        });


        byequ.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked==true && amount.getText().toString().length()>0 && ppl.getText().toString().length()>0){
                    Log.e("----- Dividing equally", "-----");
                    int n = Integer.parseInt(ppl.getText().toString());
                    double am = Double.parseDouble(amount.getText().toString().substring(currency.length()));
                    double arr[] = new double[n];
                    arr = divideEqually(n,am);
                    totalamount = am;
                    divides = arr;
                    numberofppl = n;
                    results.removeAllViews();
                    tViews.clear();
                    tEdits.clear();
                    showSplit(3,n,arr,am);
                }
            }
        });

    }


    double[] divideEqually(int n, double amount){
        double arr[] = new double[n];
        double am = amount/n;
        String t  = String.format("%.2f", am);
        am = Double.parseDouble(t);
        Log.e("AMOUNTS ARE - ", ""+am);

        for(int i=0;i<n;i++)
            arr[i] = am;

        return arr;
    }

    double[] dividePercentages(int n, double amount){
        double arr[] = new double[n];
        double pers = 100.00/n;
        String t  = String.format("%.2f", pers);
        pers = Double.parseDouble(t);

        for(int i=0;i<n;i++)
            arr[i] = pers;

        return arr;
    }



    void showSplit(int mode,int n, double[] arr, double am){

        ppl.clearFocus();
        amount.clearFocus();
        results.setFocusable(true);

        TextView textView = null;



        if(mode==3){
            for (int i = 0; i < n; i++)
            {

                textView = new TextView(this);
                String t = "Person "+(i+1)+" pays "+currency+""+arr[i];
                textView.setText(t);

                textView.setTextColor(Color.parseColor("#555555"));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
                textView.setPadding(30, 25, 30, 25);
                LinearLayout.LayoutParams lp2 =
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp2.setMargins(30, 25, 30, 25);

                textView.setLayoutParams(lp2);

                View divider = new View(getApplicationContext());
                LinearLayout.LayoutParams lp =
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
                lp.setMargins(30, 15, 30, 15);
                divider.setLayoutParams(lp);
                divider.setBackgroundColor(Color.parseColor("#c2c0c0"));

                results.addView(textView);
                if(i<n-1)
                    results.addView(divider);

                tViews.add(textView);

            }

            results.setVisibility(View.VISIBLE);
            conc_text.setText("Amount has been split equally.");
            conclusion.setVisibility(View.VISIBLE);
        }else if(mode==1){
            for (int i = 0; i < n; i++)
            {

                LinearLayout nRow = new LinearLayout(getApplicationContext());
                LinearLayout.LayoutParams lp3 =
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp3.setMargins(30, 25, 30, 25);
                nRow.setPadding(30, 25, 30, 25);
                nRow.setLayoutParams(lp3);


                nRow.setOrientation(LinearLayout.HORIZONTAL);


                textView = new TextView(this);
                String t = "Person "+(i+1)+" pays "+currency+""+(arr[i]/100 * am) ;
                textView.setText(t);


                textView.setTextColor(Color.parseColor("#555555"));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);

                TableRow.LayoutParams params = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.35f);
                textView.setLayoutParams(params);

                nRow.addView(textView);


                String t2 = ""+arr[i];
                EditText pers = new EditText(getApplicationContext());
                pers.setTextColor(Color.parseColor("#555555"));
                pers.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                pers.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);

                TableRow.LayoutParams params2 = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.50f);
                params2.gravity = Gravity.CENTER_VERTICAL;
                pers.setLayoutParams(params2);
                pers.setText(t2);
                pers.setGravity(Gravity.CENTER);
                nRow.addView(pers);

                TextView fmt = new TextView(getApplicationContext());
                TableRow.LayoutParams params3 = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.05f);
                params3.gravity = Gravity.CENTER_VERTICAL;
                pers.setLayoutParams(params3);
                fmt.setText("%");
                fmt.setTextColor(Color.parseColor("#555555"));
                fmt.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
                nRow.addView(fmt);

                View divider = new View(getApplicationContext());
                LinearLayout.LayoutParams lp =
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
                lp.setMargins(30, 15, 30, 15);
                divider.setLayoutParams(lp);
                divider.setBackgroundColor(Color.parseColor("#c2c0c0"));

                results.addView(nRow);

                if(i<n-1)
                    results.addView(divider);



                tViews.add(textView);
                tEdits.add(pers);

            }

            results.setVisibility(View.VISIBLE);
            findConclusion(totalamount, divides);
            conclusion.setVisibility(View.VISIBLE);


            for(int i=0;i<tEdits.size();i++){
                tEdits.get(i).addTextChangedListener(watcher);
                tEdits.get(i).setTag("3");
                tEdits.get(i).setId(i);
            }



        }

    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //YOUR CODE
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //YOUR CODE
        }

        @Override
        public void afterTextChanged(Editable s) {
            String outputedText = s.toString();
            Double d = Double.parseDouble(outputedText);
            String t = String.format("%.2f", d);
            d = Double.parseDouble(t);

            View v = getCurrentFocus();
            int id = v.getId();

            divides[id] = d;

            double hepays = totalamount*d/100;

            double remaining = totalamount-hepays;
            tViews.get(id).setText("Person "+(id+1)+" pays "+currency+""+hepays);
            findConclusion(totalamount, divides);


           // int tag = Integer.parseInt(v.getTag().toString());

           // showSplit(tag,numberofppl,divides,totalamount);
            //mOutputText.setText(outputedText);

        }
    };



    void findConclusion(double amount, double arr[]){
        double bal = amount;
        for(int i=0;i<arr.length;i++){
            double temp = arr[i]*amount/100;
            bal = bal-temp;
        }

        if(bal>=0){
            conc_text.setText("Amount Left: "+currency+""+bal);
        }else{
            conc_text.setText("Extra Amount Paid: "+currency+""+(bal*-1));
        }

    }

}
