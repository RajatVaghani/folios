package cybersky.folios;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import cybersky.folios.adapters.TagsListAdapter;
import cybersky.folios.helper.DatabaseHelper;

public class TagsActivity extends AppCompatActivity {


    ListView list;
    DatabaseHelper databaseHelper;

    String []tags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tags);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("All Tags Used");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));

        list = (ListView)findViewById(R.id.list);
        databaseHelper = new DatabaseHelper(getApplicationContext());

        tags = databaseHelper.getAllTags();

        list.setAdapter(new TagsListAdapter(TagsActivity.this,tags));


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent in = new Intent(TagsActivity.this, SearchableActivity.class);
                in.setAction(Intent.ACTION_SEARCH);
                in.putExtra(SearchManager.QUERY, tags[position]);
                startActivity(in);
            }
        });

    }

}
