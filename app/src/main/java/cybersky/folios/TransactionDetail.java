package cybersky.folios;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

import cybersky.folios.adapters.Tags;
import cybersky.folios.helper.DatabaseHelper;
import cybersky.folios.helper.TagsCompletionView;

public class TransactionDetail extends AppCompatActivity {

    private static final String[] MONTH_NAMES = {"January", "February", "March", "April", "May", "June", "July", "August", "September",  "October", "November", "December"};

    TextView date,amount,type,folioname,details;
    TagsCompletionView tags;
    int account_id;

    LinearLayout canva;
    String currency;

    DatabaseHelper databaseHelper;
    TinyDB tinyDB;

    List<Accounts> allist;

    Accounts account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Transaction Details");
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_transaction_detail);

        canva = (LinearLayout)findViewById(R.id.canvas);

        databaseHelper = new DatabaseHelper(getApplicationContext());
        allist = databaseHelper.getAllAccounts(1);

        for(int i=0;i<allist.size();i++){
            Log.e("---------", "---------");
            Log.e(""+allist.get(i).getDetails(), ""+allist.get(i).getId());
            Log.e("---------", "---------");
        }

        tinyDB = new TinyDB(getApplicationContext());

        account_id = getIntent().getExtras().getInt("id");
        Log.e("ACcount id", "" + account_id);

        account = databaseHelper.getSingleAccount(account_id);

        date = (TextView)findViewById(R.id.date);
        amount = (TextView)findViewById(R.id.amount);
        type = (TextView)findViewById(R.id.type);
        folioname = (TextView)findViewById(R.id.folioname);
        details = (TextView)findViewById(R.id.details);

        tags = (TagsCompletionView)findViewById(R.id.tags);

        String day = account.getDay()+""+getDayNumberSuffix(account.getDay());
        String mon = MONTH_NAMES[account.getMonth()];

        date.setText(day + " " + mon + ", " + account.getYear());

        currency = tinyDB.getString("currency");

        amount.setText(currency+account.getAmount());

        type.setText(account.getType());
        Books book;
        book = databaseHelper.getSingleBook(account.getBook_id());
        folioname.setText(book.getName());

        details.setText(account.getDetails());

        String tagsarr[] = account.getTags().split(",");
        int c=0;
        for(int i=0;i<tagsarr.length;i++){
            if(tagsarr[i].trim().equalsIgnoreCase("")){

            }else {
                Tags tag = new Tags(tagsarr[i]);
                tags.addObject(tag);
                c++;
                Log.e("tag", tagsarr[i]);
            }
        }

        tags.allowCollapse(false);
        tags.setTokenLimit(c);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_transaction_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {

            new MaterialDialog.Builder(TransactionDetail.this)
                    .title("Delete transaction")
                    .content("Are you sure?")
                    .positiveText("Yes!")
                    .titleColor(Color.parseColor("#333333"))
                    .contentColor(Color.parseColor("#474747"))
                    .negativeText("No!")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            Books book = databaseHelper.getSingleBook(account.getBook_id());
                            double bal= book.getBalance();
                            double am = account.getAmount();
                            String ty = account.getType();
                            if(ty.equalsIgnoreCase("expense")){
                                bal = bal+am;
                            }else{
                                bal = bal-am;
                            }
                            book.setBalance(bal);
                            databaseHelper.updateBook(book);
                            databaseHelper.deleteAccount(account_id);
                            Intent i = new Intent(TransactionDetail.this, MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                            dialog.dismiss();
                            startActivity(i);
                            finish();
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            dialog.dismiss();
                        }
                    })
                    .show();

            return true;
        }else if(id== R.id.action_edit){
            Intent i = new Intent(TransactionDetail.this, NewTransaction.class);
            i.putExtra("edit", account.getId());
            i.putExtra("id", account.getBook_id());
            startActivity(i);
        }else if(id == R.id.action_share){
            new MaterialDialog.Builder(TransactionDetail.this)
                    .title("Share transaction")
                    .content("How would you like to share?")
                    .positiveText("Message")
                    .titleColor(Color.parseColor("#333333"))
                    .contentColor(Color.parseColor("#474747"))
                    .negativeText("Image")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            String shareBody = "I made an " + account.getType().toLowerCase(Locale.ENGLISH) + " for '" + account.getDetails()+ "' of " + currency + account.getAmount();
                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent.setType("text/plain");
                            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Folios - The Simple Accounts");
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                            startActivity(Intent.createChooser(sharingIntent, "Share using"));
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            Bitmap bm = takeScreenshot();
                            saveBitmap(bm);
                            dialog.dismiss();
                        }
                    })
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    private String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public Bitmap takeScreenshot()
    {
        View rootView = canva.getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

    public void saveBitmap(Bitmap bitmap)
    {


            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");

            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "Folios Screenshot");
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            Uri uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    values);


            OutputStream outstream;
            try {
                outstream = getContentResolver().openOutputStream(uri);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outstream);
                outstream.close();
            } catch (Exception e) {
                System.err.println(e.toString());
            }

            share.putExtra(Intent.EXTRA_STREAM, uri);
            share.putExtra(android.content.Intent.EXTRA_SUBJECT, "Folios - The Simple Accounts");
            share.putExtra(android.content.Intent.EXTRA_TEXT, "I made an " + account.getType().toLowerCase(Locale.ENGLISH) + " for '" + account.getDetails()+ "' of " + currency + account.getAmount() );
            startActivity(Intent.createChooser(share, "Share Image"));

    }
}
