package cybersky.folios;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Time;
import android.text.method.CharacterPickerDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import cybersky.folios.helper.DatabaseHelper;

public class TransferAmount extends AppCompatActivity {


    TinyDB tinyDB;
    String currency;

    Spinner source,destination;
    int sourceSel, destinationSel;
    List<Books> books;

    DatabaseHelper databaseHelper;

    EditText amount;
    LinearLayout rootelement,head,amountlayout;

    TextView from,tof;

    LinearLayout fabsave;

    String arr[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_amount);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Transfer between Folios");

        databaseHelper = new DatabaseHelper(getApplicationContext());

        rootelement = (LinearLayout)findViewById(R.id.rootelement);
        head = (LinearLayout)findViewById(R.id.headd);
        amountlayout = (LinearLayout)findViewById(R.id.amountlayout);
        head.setVisibility(View.INVISIBLE);
        amountlayout.setVisibility(View.INVISIBLE);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        fabsave = (LinearLayout)findViewById(R.id.myFABsave);
        fabsave.setVisibility(View.INVISIBLE);

        from = (TextView)findViewById(R.id.fromname);
        tof = (TextView)findViewById(R.id.toname);

        amount = (EditText)findViewById(R.id.amount);

        tinyDB = new TinyDB(getApplicationContext());
        currency = tinyDB.getString("currency");
        books = new ArrayList<Books>();
        books = databaseHelper.getAllBooks();
        arr = new String[books.size()+1];
        arr[0] = "Select a Folio";
        for(int i=1;i<=books.size();i++){
            if(books.get(i-1).getName()!=null) {
                arr[i] = books.get(i-1).getName().toString();
                Log.e("Found ", "" + books.get(i-1).getName().toString());
            }
        }


        sourceSel = destinationSel = 0;
        source = (Spinner)findViewById(R.id.source);
        destination = (Spinner)findViewById(R.id.destination);

        ArrayAdapter<String> adapterSource = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, arr);


        // Apply the adapter to the spinner
        source.setAdapter(adapterSource);
        destination.setAdapter(adapterSource);


        fabsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int from = source.getSelectedItemPosition() - 1;
                int to = destination.getSelectedItemPosition() - 1;

                Double amo = 0.0;
                int m = 0;

                if (amount.getText().toString().length() > 0) {
                    try {
                        String t = amount.getText().toString();
                        currency = tinyDB.getString("currency");
                        //Toast.makeText(getApplicationContext(), currency, Toast.LENGTH_LONG).show();
                        //t = t.replaceAll(currency, "");
                        t = t.substring(currency.length());
                        amo = Double.parseDouble(t);
                        if (amo != 0.0) {
                            m = 1;

                        } else {
                            m = 0;
                            Snackbar.make(rootelement, "Please fill in all the fields.", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                    }
                }

                if(m==1){
                    Time time = new Time(Time.getCurrentTimezone());
                    time.setToNow();
                    String t = String.valueOf(time);
                    String yr = "" + time.year;
                    String month = "" + (time.month);
                    String date = "" + time.monthDay;
                    String tim = time.format("%k:%M:%S");

                    Books bookFrom = books.get(from);
                    Books bookTo = books.get(to);


                    Accounts fromAccount = new Accounts();
                    fromAccount.setAmount(amo);
                    fromAccount.setBook_id(from);
                    fromAccount.setDetails("Transferred to " + arr[to + 1]);
                    fromAccount.setExtra("");
                    fromAccount.setType("EXPENSE");
                    fromAccount.setTags("Transfer");
                    fromAccount.setDay(Integer.parseInt(date));
                    fromAccount.setMonth(Integer.parseInt(month));
                    fromAccount.setYear(Integer.parseInt(yr));
                    fromAccount.setTime(tim);

                    Accounts toAccount = new Accounts();
                    toAccount.setAmount(amo);
                    toAccount.setBook_id(to);
                    toAccount.setDetails("Transferred from " + arr[from + 1]);
                    toAccount.setExtra("");
                    toAccount.setType("INCOME");
                    toAccount.setTags("Transfer");
                    toAccount.setDay(Integer.parseInt(date));
                    toAccount.setMonth(Integer.parseInt(month));
                    toAccount.setYear(Integer.parseInt(yr));
                    toAccount.setTime(tim);

                    bookFrom.setBalance(bookFrom.getBalance() - amo);
                    bookTo.setBalance(bookTo.getBalance() + amo);

                    databaseHelper.updateBook(bookFrom);
                    databaseHelper.updateBook(bookTo);
                    databaseHelper.createAccounts(fromAccount, bookFrom.getId());
                    databaseHelper.createAccounts(toAccount, bookTo.getId());

                    Intent i = new Intent(TransferAmount.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();

                }

            }
        });


        source.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);

                if (position == 0) {
                    head.setVisibility(View.INVISIBLE);
                    fabsave.setVisibility(View.INVISIBLE);
                    amountlayout.setVisibility(View.INVISIBLE);
                } else if (source.getSelectedItemPosition() == destination.getSelectedItemPosition()) {
                    Snackbar.make(rootelement, "Source and target cannot be the same", Snackbar.LENGTH_SHORT).show();
                    source.setSelection(0);
                    head.setVisibility(View.INVISIBLE);
                    fabsave.setVisibility(View.INVISIBLE);
                    amountlayout.setVisibility(View.INVISIBLE);
                } else if (destination.getSelectedItemPosition() != 0) {
                    from.setText(source.getSelectedItem().toString());
                    tof.setText(destination.getSelectedItem().toString());
                    head.setVisibility(View.VISIBLE);
                    fabsave.setVisibility(View.VISIBLE);
                    amountlayout.setVisibility(View.VISIBLE);
                }


            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });


        destination.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                  @Override
                                                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                      ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                                                      if (position == 0) {
                                                          head.setVisibility(View.INVISIBLE);
                                                          fabsave.setVisibility(View.INVISIBLE);
                                                          amountlayout.setVisibility(View.INVISIBLE);
                                                      } else if (source.getSelectedItemPosition() == destination.getSelectedItemPosition()) {
                                                          Snackbar.make(rootelement, "Source and target cannot be the same", Snackbar.LENGTH_SHORT).show();
                                                          destination.setSelection(0);
                                                          head.setVisibility(View.INVISIBLE);
                                                          fabsave.setVisibility(View.INVISIBLE);
                                                          amountlayout.setVisibility(View.INVISIBLE);
                                                      } else if (source.getSelectedItemPosition() != 0) {
                                                          from.setText(source.getSelectedItem().toString());
                                                          tof.setText(destination.getSelectedItem().toString());
                                                          head.setVisibility(View.VISIBLE);
                                                          fabsave.setVisibility(View.VISIBLE);
                                                          amountlayout.setVisibility(View.VISIBLE);
                                                      }
                                                  }


                                                  @Override
                                                  public void onNothingSelected(AdapterView<?> parent) {

                                                  }
                                              }

        );





        amount.setRawInputType(Configuration.KEYBOARD_12KEY);
        amount.addTextChangedListener(new TextWatcher() {
            DecimalFormat dec = new DecimalFormat("0.00");
            String current = "";

            @Override
            public void afterTextChanged(Editable arg0) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (!s.toString().equals(current)) {
                    amount.removeTextChangedListener(this);
                    String replaceable = String.format("[%s,.\\s]", currency);

                    String cleanString = s.toString().replaceAll(replaceable, "");

                    double parsed = Double.parseDouble(cleanString);
                    // String formatted = dec.format(parsed);


                    double percen = parsed / 100;
                    current = currency + dec.format(percen);

                    amount.setText(current);
                    amount.setSelection(current.length());

                    amount.addTextChangedListener(this);
                }


            }
        });

        }

    public  void swap(View v){

        if(source.getSelectedItemPosition()==0)
            Snackbar.make(rootelement,"Please select your source", Snackbar.LENGTH_SHORT).show();
        else if(destination.getSelectedItemPosition()==0)
            Snackbar.make(rootelement, "Please select your target", Snackbar.LENGTH_SHORT).show();
        else if(destination.getSelectedItemPosition()==source.getSelectedItemPosition())
            Snackbar.make(rootelement, "Source and target cannot be the same", Snackbar.LENGTH_SHORT).show();
        else {
            int sourceIndex = source.getSelectedItemPosition();
            sourceSel=destination.getSelectedItemPosition();
            destinationSel = source.getSelectedItemPosition();
            source.setSelection(destination.getSelectedItemPosition());
            destination.setSelection(sourceIndex);
        }
    }
}
