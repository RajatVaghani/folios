package cybersky.folios;

/**
 * Created by Rajat on 9/20/2015.
 */
public class User {

        int id;
        String name, image,email;

    public  User (){

    }

    public User(int id, String name, String image, String email){
        this.id = id;
        this.name = name;
        this.image = image;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
