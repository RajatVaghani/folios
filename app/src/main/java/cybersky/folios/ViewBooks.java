package cybersky.folios;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.afollestad.materialdialogs.MaterialDialog;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cybersky.folios.adapters.DailyAdapter;
import cybersky.folios.helper.DatabaseHelper;
import cybersky.folios.helper.DatePickerFragment;
import cybersky.folios.helper.IabHelper;
import cybersky.folios.helper.IabResult;
import cybersky.folios.helper.Inventory;
import cybersky.folios.helper.Purchase;

public class ViewBooks extends AppCompatActivity{

    int bookid;
    Books books;
    DatabaseHelper databaseHelper;
    List<Accounts> accounts;
    List<Accounts> filteredaccounts;


    TextView balance;
    ImageView removeFilter;

    boolean flag_loading=false;

    LinearLayout rootla;

    int vs;

    ListView listView;

    TinyDB tinyDB;


    //JUMP TO DATE
    private int year;
    private int month;
    private int day;



    //INAPP BILLING
    String tt;
    IabHelper mHelper;
    static final String ITEM_SKU_HALF_YEAR = "half_year";
    static final String ITEM_SKU_LIFETIME = "lifetime";
    static final String ITEM_SKU_MONTHLY = "one_month";
    static final String ITEM_SKU_YEARLY = "yearly";
    static String ITEM_SKU = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_books);

        databaseHelper = new DatabaseHelper(getApplicationContext());
        tinyDB = new TinyDB(getApplicationContext());

        bookid = getIntent().getExtras().getInt("id");
        books = new Books();
        books = databaseHelper.getSingleBook(bookid);
        accounts = databaseHelper.getAllAccounts(bookid);

        rootla = (LinearLayout)findViewById(R.id.root);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(books.getName().toUpperCase(Locale.ENGLISH));
        setSupportActionBar(toolbar);
        toolbar.inflateMenu(R.menu.menu_view_books);

        listView = (ListView)findViewById(R.id.listView);
        balance = (TextView)findViewById(R.id.balance);

        String base64EncodedPublicKey =
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqY0b5oNMey58PytiHwt2PmPa4oUlCslzWZ+S6cQ4km6DcpwMoFUpCZHRg+zs+8w0O683eieck5csmndWX+qNF3GPO5UKYqWlMXlHEECfnJTIr5Zj5wlPFxyhGIBss30ZorXdrNjIxr5gLgGUuq0xGwQEaphW6SWDiQbUb0kfASMT860Yw955qJU6v1eY5Aw6pEqUciLPPbqmeU8LbAs3O5QeXMTpM4jWmv16urAkNaoU1Su3p/o281KUXmb4KiYFalHM7gHt2N/srtN+BxlHRvO0QIeYTLtrMsd5+QYcN88GcTnrL3r88djk4R2kleBm91kvC8MYw/yUpsgLnqsZwwIDAQAB";

        mHelper = new IabHelper(ViewBooks.this, base64EncodedPublicKey);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d("TAG", "In-app Billing setup failed: " + result);
                } else {
                    Log.d("TAG", "In-app Billing is set up OK");
                }
            }
        });

        removeFilter = (ImageView)findViewById(R.id.removeFilter);

        final Calendar c = Calendar.getInstance();
        year  = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day   = c.get(Calendar.DAY_OF_MONTH);

        balance.setText("Balance: " + tinyDB.getString("currency") + books.getBalance());

        vs = tinyDB.getInt("default", 0);

        if(vs==1)
            listView.setAdapter(new DailyAdapter(ViewBooks.this, accounts, books));


        removeFilter.setVisibility(View.INVISIBLE);

        removeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                balance.setText("Balance: " + tinyDB.getString("currency") + books.getBalance());
                removeFilter.setVisibility(View.INVISIBLE);
                accounts = databaseHelper.getAllAccounts(bookid);
                listView.setAdapter(new DailyAdapter(ViewBooks.this, accounts, books));
                Snackbar.make(rootla,"Filter Removed!", Snackbar.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_books, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_create) {
            Intent i = new Intent(getApplicationContext(), NewTransaction.class);
            i.putExtra("id", bookid);
            i.putExtra("edit", 0);
            startActivity(i);
            return true;
        }else if(id == R.id.action_pdf){
            // ---------------------------------------------
            // CHANGE FOR PREMIUM USERS
            // 0 = Not Premium
            // 1 = Premium
            // ---------------------------------------------
            if(tinyDB.getInt("inapp",0) == 1 ) {
                ExportDatabasePDFTask exportDatabasePDFTask = new ExportDatabasePDFTask();
                exportDatabasePDFTask.execute();
            }else{
                User user = databaseHelper.getUser(1);


                final String tok = user.getName() + "_" + user.getEmail();

                String ops[] = {"Monthly Subscription", "Half-Yearly Subscription", "Yearly Subscription", "Lifetime Subscription"};

                new MaterialDialog.Builder(ViewBooks.this)
                        .title("This is a premium feature!")
                        .items(ops)
                        .itemsCallback(new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                                if (which == 0) {
                                    mHelper.launchPurchaseFlow(ViewBooks.this, ITEM_SKU_MONTHLY, 10001,
                                            mPurchaseFinishedListener, tok);
                                    ITEM_SKU = ITEM_SKU_MONTHLY;
                                } else if (which == 1) {
                                    mHelper.launchPurchaseFlow(ViewBooks.this, ITEM_SKU_HALF_YEAR, 10001,
                                            mPurchaseFinishedListener, tok);
                                    ITEM_SKU = ITEM_SKU_HALF_YEAR;
                                } else if (which == 2) {
                                    mHelper.launchPurchaseFlow(ViewBooks.this, ITEM_SKU_YEARLY, 10001,
                                            mPurchaseFinishedListener, tok);
                                    ITEM_SKU = ITEM_SKU_YEARLY;
                                } else if (which == 3) {
                                    mHelper.launchPurchaseFlow(ViewBooks.this, ITEM_SKU_LIFETIME, 10001,
                                            mPurchaseFinishedListener, tok);
                                    ITEM_SKU = ITEM_SKU_LIFETIME;
                                }

                            }
                        })
                        .show();
            }
            return true;
        }else if(id == R.id.action_jump){

            String ops[] = {"By specific date", "By specific tag", "By specific description"};

            new MaterialDialog.Builder(ViewBooks.this)
                    .title("Filter Transactions")
                    .items(ops)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                            if (which == 0) {
                                dialog.dismiss();

                                DatePickerFragment datePickerFragment = new DatePickerFragment();
                                datePickerFragment.setCallBack(ondate);

                                DialogFragment newFragment = datePickerFragment;
                                newFragment.show(getSupportFragmentManager(), "datePicker");



                            } else if (which == 1) {
                                dialog.dismiss();
                                new MaterialDialog.Builder(ViewBooks.this)
                                        .title("Filter by Tags")
                                        .content("Enter the tag you want to search using")
                                        .inputType(InputType.TYPE_CLASS_TEXT)
                                        .input("", "", new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                                // Do something
                                                ArrayList<Accounts> filtered = new ArrayList<Accounts>();
                                                accounts = databaseHelper.getAllAccounts(books.getId());
                                                for (int i = 0; i < accounts.size(); i++) {
                                                    if (accounts.get(i).getTags().contains(input))
                                                        filtered.add(accounts.get(i));
                                                }

                                                listView.setAdapter(new DailyAdapter(ViewBooks.this, filtered, books));

                                                balance.setText("Filtered using '" + input + "'");
                                                removeFilter.setVisibility(View.VISIBLE);
                                                Snackbar.make(rootla,"Filter Added!", Snackbar.LENGTH_SHORT).show();
                                                dialog.dismiss();
                                            }
                                        }).show();
                            } else if (which == 2) {
                                dialog.dismiss();
                                new MaterialDialog.Builder(ViewBooks.this)
                                        .title("Filter by Description")
                                        .content("Enter the description keywords you want to search using")
                                        .inputType(InputType.TYPE_CLASS_TEXT)
                                        .input("", "", new MaterialDialog.InputCallback() {
                                            @Override
                                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                                // Do something
                                                ArrayList<Accounts> filtered = new ArrayList<Accounts>();
                                                accounts = databaseHelper.getAllAccounts(books.getId());
                                                for (int i = 0; i < accounts.size(); i++) {
                                                    if (accounts.get(i).getDetails().toLowerCase(Locale.ENGLISH).contains(input.toString().toLowerCase(Locale.ENGLISH)))
                                                        filtered.add(accounts.get(i));
                                                }

                                                listView.setAdapter(new DailyAdapter(ViewBooks.this, filtered, books));

                                                balance.setText("Filtered using '" + input + "'");
                                                removeFilter.setVisibility(View.VISIBLE);
                                                Snackbar.make(rootla,"Filter Added!", Snackbar.LENGTH_SHORT).show();
                                                dialog.dismiss();
                                            }
                                        }).show();
                            }

                        }
                    })
                    .show();

        }

        return super.onOptionsItemSelected(item);
    }


    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {


            ArrayList<Accounts> filtered = new ArrayList<Accounts>();
            accounts = databaseHelper.getAllAccounts(books.getId());
            for (int i = 0; i < accounts.size(); i++) {
                if (accounts.get(i).getDay() == dayOfMonth && accounts.get(i).getMonth() == monthOfYear && accounts.get(i).getYear() == year)
                    filtered.add(accounts.get(i));
            }


            balance.setText("Filtered using '"+dayOfMonth+"/"+(monthOfYear+1)+"/"+year+"'");
            removeFilter.setVisibility(View.VISIBLE);
            Snackbar.make(rootla,"Filter Added!", Snackbar.LENGTH_SHORT).show();
            listView.setAdapter(new DailyAdapter(ViewBooks.this, filtered, books));
        }



    };

    public class ExportDatabasePDFTask extends AsyncTask<String, Void, Boolean> {
        private final ProgressDialog dialog = new ProgressDialog(ViewBooks.this);

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Generating PDF ...");
            this.dialog.show();
        }

        protected Boolean doInBackground(final String... args) {
            try{

                Document document = new Document(PageSize.A4);
                PdfWriter.getInstance(document, new FileOutputStream(Environment.getExternalStorageDirectory() + "/Folios/" + books.getName() + "Folio.pdf"));
                document.open();

                String bookdesc = "Total balance left: "+tinyDB.getString("currency")+books.getBalance();
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));
                document.add(new Paragraph("All Transactions for "+books.getName()+ " Folio"));
                document.add(new Paragraph(bookdesc));
                document.add(new Paragraph(" "));
                document.add(new Paragraph(" "));

                Font smallBold = new Font(Font.FontFamily.HELVETICA, 15,
                        Font.BOLD, BaseColor.WHITE);

                PdfPTable table = new PdfPTable(4);
                PdfPCell cell = new PdfPCell(new Paragraph ("ID",smallBold));
                cell.setBackgroundColor(new BaseColor(63, 81, 181));
                cell.setPadding(10.0f);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("AMOUNT",smallBold));
                cell.setBackgroundColor(new BaseColor(63, 81, 181));
                cell.setPadding(10.0f);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("DETAILS",smallBold));
                cell.setBackgroundColor(new BaseColor(63, 81, 181));
                cell.setPadding(10.0f);
                table.addCell(cell);

                cell = new PdfPCell(new Paragraph("DATE",smallBold));
                cell.setBackgroundColor(new BaseColor(63, 81, 181));
                cell.setPadding(10.0f);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                accounts = databaseHelper.getAllAccounts(books.getId());

                Log.e("ACCOUNTS SIZE", "" + accounts.size());



                for(int i =0;i<accounts.size();i++){



                    cell = new PdfPCell(new Paragraph(""+(i+1)));
                    cell.setPadding(3.0f);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);


                    if(accounts.get(i).getType().equalsIgnoreCase("expense"))
                        table.addCell("-"+tinyDB.getString("currency")+accounts.get(i).getAmount());
                    else
                        table.addCell("+"+tinyDB.getString("currency")+accounts.get(i).getAmount());


                    table.addCell(""+accounts.get(i).getDetails());


                    String dat="";
                    dat += accounts.get(i).getDay()+"/";
                    dat += (accounts.get(i).getMonth()+1)+"/";
                    dat += accounts.get(i).getYear();
                    cell = new PdfPCell(new Paragraph(dat));
                    cell.setPadding(3.0f);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);

                }


                document.add(table);
                document.close();

                return true;
            } catch (Exception e) {
                Log.e("ViewBooks", e.getMessage(), e);
                return false;
            }
        }

        protected void onPostExecute(final Boolean success) {
            if (this.dialog.isShowing()) { this.dialog.dismiss(); }
            if (success) {
                Snackbar.make(rootla, "PDF Generated at " + Environment.getExternalStorageDirectory() + "/Folios/" + books.getName() + "Folio.pdf", Snackbar.LENGTH_LONG).show();
            } else {
                Toast.makeText(ViewBooks.this, "Export failed", Toast.LENGTH_SHORT).show();
            }
        }
    }



















    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data)
    {
        if (!mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase)
        {
            if (result.isFailure()) {
                // Handle error
                return;
            }
            else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();
                //Toast.makeText(getApplicationContext(),"PURCHASE SUCCESFUL", Toast.LENGTH_SHORT).show();
            }

        }
    };

    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // Handle failure
            } else {
                mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                        mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {

                        tt = purchase.getSku();

                        createInApp createInApp =  new createInApp();
                        createInApp.execute();


                    } else {
                        // handle error
                    }
                }
            };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }











    class createInApp extends AsyncTask<Void, Void, Void> {

        ProgressDialog asyncDialog = new ProgressDialog(ViewBooks.this);

        @Override
        protected void onPreExecute() {
            asyncDialog.setMessage("Completing Purchase ...");
            asyncDialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // Show response on activity
            asyncDialog.dismiss();

            Toast.makeText(getApplicationContext(), "Purchase Successful", Toast.LENGTH_LONG).show();
            tinyDB.putInt("inapp", 1);
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create data variable for sent values to server

            /*String name = databaseHelper.getUser(1).getName();
            String email = databaseHelper.getUser(1).getEmail();

            String data="";

            try {
                data = URLEncoder.encode("email", "UTF-8")
                        + "=" + URLEncoder.encode(email, "UTF-8");

                data += "&" + URLEncoder.encode("type", "UTF-8") + "="
                        + URLEncoder.encode(tt, "UTF-8");

                data += "&" + URLEncoder.encode("name", "UTF-8") + "="
                        + URLEncoder.encode(name, "UTF-8");
                // Get user defined values

            }catch (Exception e){

            }


            String text = "";
            BufferedReader reader=null;

            // Send data
            try
            {

                // Defined URL  where to send data
                URL url = new URL("http://rvcv.in/folios/insert_purchase.php");

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( data );
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "\n");
                }


                text = sb.toString();
            }
            catch(Exception ex)
            {

            }
            finally
            {
                try
                {

                    reader.close();
                }

                catch(Exception ex) {}
            }
*/

            return null;
        }
    }


}
