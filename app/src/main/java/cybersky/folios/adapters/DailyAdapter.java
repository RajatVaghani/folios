package cybersky.folios.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cybersky.folios.Accounts;
import cybersky.folios.Books;
import cybersky.folios.NewTransaction;
import cybersky.folios.R;
import cybersky.folios.TinyDB;
import cybersky.folios.ViewBooks;
import cybersky.folios.helper.DatabaseHelper;

public class DailyAdapter extends BaseAdapter {

    Context context;
    List<Accounts> accounts;
    DatabaseHelper databaseHelper;
    Books book;
    TinyDB tinyDB;
    List<Accounts> allaccounts;
    List<List> fullist;
    List<Integer> days;
    List<Integer> months;
    List<Integer> years;
    private static final String[] MONTH_NAMES = {"January", "February", "March", "April", "May", "June", "July", "August", "September",  "October", "November", "December"};

    public DailyAdapter(Context con, List<Accounts>acc, Books bo){
        context = con;
        accounts = acc;
        book = bo;
        tinyDB = new TinyDB(context);
        databaseHelper = new DatabaseHelper(context);

        Log.e("RECEIVED SIZE", ""+accounts.size());



        fullist = new ArrayList<>();
        days = new ArrayList<>();
        months = new ArrayList<>();
        years = new ArrayList<>();
        int pos = 0;
        while(accounts.size()!=0){
            allaccounts = new ArrayList<>();
            int i=accounts.size()-1;
            int day = accounts.get(i).getDay();
            int month = accounts.get(i).getMonth();
            int year = accounts.get(i).getYear();
            allaccounts.add(accounts.get(i));
            Log.e("COMPARING ACCOUNT " + accounts.get(i).getDetails(), "---- " + accounts.get(i).getDay());
            accounts.remove(i);
            for(int j=i-1;j>=0;j--){
                if(accounts.get(j).getDay() == day && accounts.get(j).getMonth()==month && accounts.get(j).getYear()==year){
                    allaccounts.add(accounts.get(j));
                    Log.e("FOUND ACCOUNT " + accounts.get(j).getDetails(), "----");
                    accounts.remove(j);
                }
            }
            fullist.add(pos,allaccounts);
            days.add(pos, day);
            months.add(pos,month);
            years.add(pos,year);
            pos++;
        }

        Log.e("TOTAL SIZE: ",""+fullist.size());

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return fullist.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return accounts.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(final int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        if(arg1==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            arg1 = inflater.inflate(R.layout.list_daily, arg2,false);
        }

        TextView date = (TextView)arg1.findViewById(R.id.date);
        TextView todays = (TextView)arg1.findViewById(R.id.todayspend);
        LinearLayout list = (LinearLayout)arg1.findViewById(R.id.transactionsholder);


        allaccounts = new ArrayList<>();
        allaccounts = fullist.get(arg0);

        double todaysspend=0.0;

            int size = allaccounts.size();

            Log.e(""+arg0+" is "+size,"LONG");


            list.removeAllViews();

            ListItemsAdapter adapter = new ListItemsAdapter(context, allaccounts);
            for (int i = 0; i <size; i++) {
                View view = adapter.getView(i, null, list);
                Log.e("Added " + allaccounts.get(i).getDetails(), " ------- ");

                if(allaccounts.get(i).getType().equalsIgnoreCase("expense")){
                    todaysspend-=allaccounts.get(i).getAmount();
                }else
                    todaysspend+=allaccounts.get(i).getAmount();

                list.addView(view);
                if(i!=size-1){
                    View divider = new View(context);
                    LinearLayout.LayoutParams lp =
                            new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
                    lp.setMargins(5, 5, 5, 5);
                    divider.setLayoutParams(lp);
                    divider.setBackgroundColor(Color.parseColor("#c2c0c0"));
                    list.addView(divider);
                }
            }

            //list.setAdapter(new LastFiveAdapter(context, topfive));

        String day = days.get(arg0)+""+getDayNumberSuffix(days.get(arg0));
        String mon = MONTH_NAMES[months.get(arg0)];


        String init="";
        if(todaysspend > 0.0){
            init="Income of ";
        }else {
            init = "Expense of ";
            todaysspend *= -1;
        }

        String cur= tinyDB.getString("currency");

        todays.setText(init + cur + todaysspend);

        date.setText(day + " " + mon + ", " + years.get(arg0));





        return arg1;
    }

    private String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

}
