package cybersky.folios.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cybersky.folios.Accounts;
import cybersky.folios.Books;
import cybersky.folios.NewTransaction;
import cybersky.folios.R;
import cybersky.folios.TinyDB;
import cybersky.folios.helper.DatabaseHelper;

public class LastFiveAdapter extends BaseAdapter {

    Context context;
    List<Accounts> accounts;
    DatabaseHelper databaseHelper;
    TinyDB tinyDB;

    public LastFiveAdapter(Context con, List<Accounts>acc){
        context = con;
        accounts = acc;
        tinyDB = new TinyDB(context);
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return accounts.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return accounts.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(final int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        if(arg1==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            arg1 = inflater.inflate(R.layout.last_five_item, arg2,false);
        }

        TextView amount = (TextView)arg1.findViewById(R.id.amount);
        TextView detail = (TextView)arg1.findViewById(R.id.desc);
        String currency = tinyDB.getString("currency");
        String t;
        if(accounts.get(arg0).getType().equalsIgnoreCase("income")){
            t = "+ " + currency;
        }else
            t = "- "+currency;
        amount.setText(t+accounts.get(arg0).getAmount());
        detail.setText(accounts.get(arg0).getDetails());


        return arg1;
    }

}
