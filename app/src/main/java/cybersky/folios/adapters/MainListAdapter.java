package cybersky.folios.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cybersky.folios.Accounts;
import cybersky.folios.Books;
import cybersky.folios.NewTransaction;
import cybersky.folios.R;
import cybersky.folios.TinyDB;
import cybersky.folios.ViewBooks;
import cybersky.folios.helper.DatabaseHelper;

public class MainListAdapter extends BaseAdapter {

    Context context;
    List<Books> books;
    DatabaseHelper databaseHelper;
    TinyDB tinyDB;

    public MainListAdapter(Context con, List<Books>acc){
        context = con;
        books = acc;
        tinyDB = new TinyDB(context);
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return books.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return books.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(final int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        if(arg1==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            arg1 = inflater.inflate(R.layout.main_list, arg2,false);
        }

        TextView account = (TextView)arg1.findViewById(R.id.account_name);
        TextView balance = (TextView)arg1.findViewById(R.id.account_bal);
        TextView msg = (TextView)arg1.findViewById(R.id.msgg);
        ImageView create = (ImageView)arg1.findViewById(R.id.newBtn);
        ImageView viewbtn = (ImageView)arg1.findViewById(R.id.viewBtn);
        //ListView list = (ListView)arg1.findViewById(R.id.transactions);
        LinearLayout list = (LinearLayout)arg1.findViewById(R.id.transactions);
        if(databaseHelper.getAccountsCount(books.get(arg0).getId())==0){
            list.setVisibility(View.GONE);
            msg.setVisibility(View.VISIBLE);
        }else{
            msg.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);

            List<Accounts> allaccounts;
            allaccounts = databaseHelper.getAllAccounts(books.get(arg0).getId());

            int size = allaccounts.size();

            List<Accounts> topfive = new ArrayList<>();


            int count=0;
            topfive.clear();
            while (count<5){
                topfive.add(allaccounts.get(size-1));
                size = size-1;
                count++;
                if(size==0)
                    break;
                Log.e("Found", ""+count);
            }

            list.removeAllViews();

            LastFiveAdapter adapter = new LastFiveAdapter(context, topfive);
            for (int i = 0; i <count; i++) {
                View view = adapter.getView(i, null, list);
                Log.e("Added " + topfive.get(i).getDetails(), " ------- to --- "+books.get(arg0).getName());
                list.addView(view);
            }

            //list.setAdapter(new LastFiveAdapter(context, topfive));



        }

        Log.e("NAME", books.get(arg0).getName());

        account.setText(books.get(arg0).getName());
        balance.setText(tinyDB.getString("currency")+String.valueOf(books.get(arg0).getBalance()));
        create.setTag(books.get(arg0).getId());


        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, NewTransaction.class);
                i.putExtra("id", books.get(arg0).getId());
                i.putExtra("edit", 0);
                context.startActivity(i);

            }
        });

        viewbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ViewBooks.class);
                i.putExtra("id", books.get(arg0).getId());
                context.startActivity(i);
            }
        });


        return arg1;
    }

}
