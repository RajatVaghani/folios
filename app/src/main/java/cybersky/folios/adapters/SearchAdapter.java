package cybersky.folios.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import cybersky.folios.Accounts;
import cybersky.folios.Books;
import cybersky.folios.R;
import cybersky.folios.TinyDB;
import cybersky.folios.TransactionDetail;
import cybersky.folios.helper.DatabaseHelper;

/**
 * Created by Rajat on 9/25/2015.
 */
public class SearchAdapter extends BaseAdapter {

    Context context;
    List<Accounts> accounts;
    DatabaseHelper databaseHelper;
    TinyDB tinyDB;

    public SearchAdapter(Context con, List<Accounts>acc){
        context = con;
        accounts = acc;
        tinyDB = new TinyDB(context);
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return accounts.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return accounts.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(final int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        if(arg1==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            arg1 = inflater.inflate(R.layout.search_item, arg2,false);
        }

        TextView amount = (TextView)arg1.findViewById(R.id.amount);
        TextView detail = (TextView)arg1.findViewById(R.id.particular);
        TextView folio = (TextView)arg1.findViewById(R.id.folioname);
        final LinearLayout transaction = (LinearLayout)arg1.findViewById(R.id.trans);

        String currency = tinyDB.getString("currency");
        String t;
        if(accounts.get(arg0).getType().equalsIgnoreCase("income")){
            t = "+ " + currency;
        }else
            t = "- "+currency;
        amount.setText(t+accounts.get(arg0).getAmount());

        Books book;
        final int iddd = accounts.get(arg0).getId();
        book = databaseHelper.getSingleBook(accounts.get(arg0).getBook_id());
        detail.setText(accounts.get(arg0).getDetails());
        folio.setText(book.getName() + " Folio");

        transaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, TransactionDetail.class);
                i.putExtra("id", accounts.get(arg0).getId());
                context.startActivity(i);
            }
        });

        return arg1;
    }

}
