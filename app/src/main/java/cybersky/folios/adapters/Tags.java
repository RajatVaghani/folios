package cybersky.folios.adapters;

import java.io.Serializable;

/**
 * Created by Rajat on 9/25/2015.
 */
public class Tags implements Serializable {

    String name;

    public Tags(String n) { name = n; }

    public String getName() { return name; }

    @Override
    public String toString() { return name; }
}