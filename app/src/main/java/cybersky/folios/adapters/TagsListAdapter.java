package cybersky.folios.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cybersky.folios.Accounts;
import cybersky.folios.R;
import cybersky.folios.TinyDB;
import cybersky.folios.TransactionDetail;
import cybersky.folios.helper.DatabaseHelper;

/**
 * Created by Rajat on 10/1/2015.
 */
public class TagsListAdapter extends BaseAdapter {

    Context context;
    String []tags;
    DatabaseHelper databaseHelper;
    TinyDB tinyDB;

    public TagsListAdapter(Context con, String []t){
        context = con;
        tags = t;
        tinyDB = new TinyDB(context);
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return tags.length;
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return tags[arg0];
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(final int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        if(arg1==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            arg1 = inflater.inflate(R.layout.tag_item, arg2,false);
        }

        TextView tag = (TextView)arg1.findViewById(R.id.tagsname);

        tag.setText(tags[arg0]);


        return arg1;
    }

}
