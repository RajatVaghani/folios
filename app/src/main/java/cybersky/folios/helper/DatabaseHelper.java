package cybersky.folios.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cybersky.folios.Accounts;
import cybersky.folios.Books;
import cybersky.folios.User;

/**
 * Created by Rajat on 9/20/2015.
 */
public class DatabaseHelper  extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "FolioDB";

    // Table Names
    private static final String TABLE_USER = "tableUSER";
    private static final String TABLE_BOOKS = "tableBOOKS";
    private static final String TABLE_ACCOUNTS = "tableACCOUNTS";

    // Common column names
    private static final String KEY_ID = "id";

    // USER Table - column nmaes
    private static final String KEY_NAME_USER = "name";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_EMAIL = "email";

    // BOOKS Table - column names
    private static final String KEY_NAME_BOOKS = "name";
    private static final String KEY_LASTUPDATED = "last_updated";
    private static final String KEY_BALANCE = "balance";

    // NOTE_TAGS Table - column names
    private static final String KEY_BOOK_ID = "book_id";
    private static final String KEY_TYPE = "type";
    private static final String KEY_TAGS = "tags";
    private static final String KEY_AMOUNT = "amount";
    private static final String KEY_DETAILS = "details";
    private static final String KEY_EXTRA = "extra";
    private static final String KEY_DAY = "day";
    private static final String KEY_MONTH = "month";
    private static final String KEY_YEAR = "year";
    private static final String KEY_TIME = "time";

    // Table Create Statements
    // Todo table create statement
    private static final String CREATE_TABLE_USER = "CREATE TABLE "
            + TABLE_USER + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME_USER
            + " TEXT," + KEY_IMAGE + " TEXT," + KEY_EMAIL
            + " TEXT" + ")";

    // Tag table create statement
    private static final String CREATE_TABLE_BOOKS = "CREATE TABLE " + TABLE_BOOKS
            + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME_BOOKS + " TEXT,"
            + KEY_LASTUPDATED + " TEXT," + KEY_BALANCE
            + " TEXT" + ")";

    // todo_tag table create statement
    private static final String CREATE_TABLE_ACCOUNTS = "CREATE TABLE "
            + TABLE_ACCOUNTS + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_BOOK_ID + " INTEGER," + KEY_TYPE + " TEXT,"
            + KEY_TAGS + " TEXT," + KEY_AMOUNT + " TEXT," + KEY_DETAILS + " TEXT," + KEY_EXTRA + " TEXT," + KEY_DAY + " TEXT," + KEY_MONTH + " TEXT," + KEY_YEAR + " TEXT," + KEY_TIME
            + " TEXT" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // creating required tables
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_BOOKS);
        db.execSQL(CREATE_TABLE_ACCOUNTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNTS);

        // create new tables
        onCreate(db);
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }







    // ------------------------ "USER" table methods ----------------//


    public long createUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME_USER, user.getName());
        values.put(KEY_IMAGE, user.getImage());
        values.put(KEY_EMAIL, user.getEmail());

        // insert row
        long id = db.insert(TABLE_USER, null, values);

        return id;
    }

    public User getUser(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_USER + " WHERE "
                + KEY_ID + " = " + id;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        User user = new User();
        user.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        user.setName((c.getString(c.getColumnIndex(KEY_NAME_USER))));
        user.setImage((c.getString(c.getColumnIndex(KEY_IMAGE))));
        user.setEmail((c.getString(c.getColumnIndex(KEY_EMAIL))));

        return user;
    }

    public void deleteUser(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USER, KEY_ID + " = ?",
                new String[]{String.valueOf(id)});
    }




    // ------------------------ "BOOKS" table methods ----------------//

    public long createBooks(Books books, int ids) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME_BOOKS, books.getName());
        values.put(KEY_LASTUPDATED, books.getLast_updated());
        values.put(KEY_BALANCE, books.getBalance());


        // insert row
        long id = db.insert(TABLE_BOOKS, null, values);

        return id;
    }

    public Books getSingleBook(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_BOOKS + " WHERE "
                + KEY_ID + " = " + id;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        Books books = new Books();
        books.setId(c.getInt(c.getColumnIndex(KEY_ID)));
        books.setName((c.getString(c.getColumnIndex(KEY_NAME_BOOKS))));
        books.setBalance(Double.parseDouble(c.getString(c.getColumnIndex(KEY_BALANCE))));
        books.setLast_updated((c.getString(c.getColumnIndex(KEY_LASTUPDATED))));
        return books;
    }

    public List<Books> getAllBooks() {
        List<Books> booksList = new ArrayList<Books>();
        String selectQuery = "SELECT  * FROM " + TABLE_BOOKS;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Books t = new Books();
                t.setId(c.getInt((c.getColumnIndex(KEY_ID))));
                t.setName((c.getString(c.getColumnIndex(KEY_NAME_BOOKS))));
                t.setBalance(Double.parseDouble(c.getString(c.getColumnIndex(KEY_BALANCE))));
                t.setLast_updated((c.getString(c.getColumnIndex(KEY_LASTUPDATED))));

                // adding to tags list
                booksList.add(t);
            } while (c.moveToNext());
        }
        return booksList;
    }

    public int updateBook(Books books) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME_BOOKS, books.getName());
        values.put(KEY_BALANCE, books.getBalance());

        // updating row
        return db.update(TABLE_BOOKS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(books.getId()) });
    }

    public void deleteBook(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BOOKS, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });
    }

    public int getBooksCount() {
        String countQuery = "SELECT  * FROM " + TABLE_BOOKS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }




    // ------------------------ "ACCOUNTS" table methods ----------------//

    public long createAccounts(Accounts accounts, long book_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_BOOK_ID, book_id);
        values.put(KEY_TYPE, accounts.getType());
        values.put(KEY_TAGS, accounts.getTags());
        values.put(KEY_AMOUNT, accounts.getAmount());
        values.put(KEY_DETAILS, accounts.getDetails());
        values.put(KEY_EXTRA, accounts.getExtra());
        values.put(KEY_DAY, accounts.getDay());
        values.put(KEY_MONTH, accounts.getMonth());
        values.put(KEY_YEAR, accounts.getYear());
        values.put(KEY_TIME, accounts.getTime());

        // insert row
        long id = db.insert(TABLE_ACCOUNTS, null, values);

        return id;
    }

    public Accounts getSingleAccount(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_ACCOUNTS + " WHERE "
                + KEY_ID + " = " + id;

        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        Log.e("FOUND SIZE", "" + c.getCount());
        Accounts accounts = new Accounts();
        if (c.moveToFirst()) {
            do {
                accounts.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                accounts.setBook_id(c.getInt(c.getColumnIndex(KEY_BOOK_ID)));
                accounts.setType(c.getString(c.getColumnIndex(KEY_TYPE)));
                accounts.setTags(c.getString(c.getColumnIndex(KEY_TAGS)));
                accounts.setAmount(Double.parseDouble(c.getString(c.getColumnIndex(KEY_AMOUNT))));
                accounts.setDetails(c.getString(c.getColumnIndex(KEY_DETAILS)));
                accounts.setExtra(c.getString(c.getColumnIndex(KEY_EXTRA)));
                accounts.setDay(c.getInt(c.getColumnIndex(KEY_DAY)));
                accounts.setMonth(c.getInt(c.getColumnIndex(KEY_MONTH)));
                accounts.setYear(c.getInt(c.getColumnIndex(KEY_YEAR)));
                accounts.setTime(c.getString(c.getColumnIndex(KEY_TIME)));
            } while (c.moveToNext());
        }
        return accounts;
    }

    public List<Accounts> searchAccounts(String tag) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_ACCOUNTS + " WHERE "
                + KEY_TAGS + " LIKE '%" + tag +"%' ORDER BY " + KEY_ID + " DESC";

        Log.e(LOG, selectQuery);

        List<Accounts> matches = new ArrayList<>();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()){
            do{
                Accounts accounts = new Accounts();
                accounts.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                accounts.setBook_id(c.getInt(c.getColumnIndex(KEY_BOOK_ID)));
                accounts.setType(c.getString(c.getColumnIndex(KEY_TYPE)));
                accounts.setTags(c.getString(c.getColumnIndex(KEY_TAGS)));
                accounts.setAmount(Double.parseDouble(c.getString(c.getColumnIndex(KEY_AMOUNT))));
                accounts.setDetails(c.getString(c.getColumnIndex(KEY_DETAILS)));
                accounts.setExtra(c.getString(c.getColumnIndex(KEY_EXTRA)));
                accounts.setDay(c.getInt(c.getColumnIndex(KEY_DAY)));
                accounts.setMonth(c.getInt(c.getColumnIndex(KEY_MONTH)));
                accounts.setYear(c.getInt(c.getColumnIndex(KEY_YEAR)));
                accounts.setTime(c.getString(c.getColumnIndex(KEY_TIME)));
                matches.add(accounts);
            }while(c.moveToNext());
        }

        
        return matches;
    }

    public List<Accounts> getAllAccounts(long book_id) {
        List<Accounts> accountsList = new ArrayList<Accounts>();
        String selectQuery = "SELECT  * FROM " + TABLE_ACCOUNTS + " WHERE "
                + KEY_BOOK_ID + " = " + book_id;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Accounts accounts = new Accounts();
                accounts.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                accounts.setBook_id(c.getInt(c.getColumnIndex(KEY_BOOK_ID)));
                accounts.setType(c.getString(c.getColumnIndex(KEY_TYPE)));
                accounts.setTags(c.getString(c.getColumnIndex(KEY_TAGS)));
                accounts.setAmount(Double.parseDouble(c.getString(c.getColumnIndex(KEY_AMOUNT))));
                accounts.setDetails(c.getString(c.getColumnIndex(KEY_DETAILS)));
                accounts.setExtra(c.getString(c.getColumnIndex(KEY_EXTRA)));
                accounts.setDay(c.getInt(c.getColumnIndex(KEY_DAY)));
                accounts.setMonth(c.getInt(c.getColumnIndex(KEY_MONTH)));
                accounts.setYear(c.getInt(c.getColumnIndex(KEY_YEAR)));
                accounts.setTime(c.getString(c.getColumnIndex(KEY_TIME)));

                // adding to tags list
                accountsList.add(accounts);
            } while (c.moveToNext());
        }
        return accountsList;
    }

    public Cursor fetchAllAccounts() {
        List<Accounts> accountsList = new ArrayList<Accounts>();
        String selectQuery = "SELECT  * FROM " + TABLE_ACCOUNTS;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();


        // looping through all rows and adding to list

        return  db.rawQuery(selectQuery, null);
    }

    public int updateAccount(Accounts accounts) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TYPE, accounts.getType());
        values.put(KEY_TAGS, accounts.getTags());
        values.put(KEY_AMOUNT, accounts.getAmount());
        values.put(KEY_DETAILS, accounts.getDetails());
        values.put(KEY_EXTRA, accounts.getExtra());
        values.put(KEY_DAY, accounts.getDay());
        values.put(KEY_MONTH, accounts.getMonth());
        values.put(KEY_YEAR, accounts.getYear());
        values.put(KEY_TIME, accounts.getTime());




        // updating row
        return db.update(TABLE_ACCOUNTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(accounts.getId()) });
    }

    public void deleteAccount(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ACCOUNTS, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });
    }

    public int getAccountsCount(long book_id) {
        String countQuery = "SELECT  * FROM " + TABLE_ACCOUNTS + " WHERE "
                + KEY_BOOK_ID + " = " + book_id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public String[] getAllTags(){
        String Query = "SELECT * FROM " + TABLE_ACCOUNTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(Query, null);

        List<String> tags = new ArrayList<>();

        if (c.moveToFirst()) {
            do {
                String tag = c.getString(c.getColumnIndex(KEY_TAGS));

                String temp[] = tag.split(",");

                for(int i=0;i<temp.length;i++){
                    if(!tags.contains(temp[i].trim()) && !temp[i].equalsIgnoreCase(""))
                        tags.add(temp[i].trim());
                }

            } while (c.moveToNext());
        }

        String full[] = new String[tags.size()];
        tags.toArray(full);
        return  full;

    }

}