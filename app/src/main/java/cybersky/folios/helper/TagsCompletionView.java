package cybersky.folios.helper;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.plus.model.people.Person;
import com.tokenautocomplete.TokenCompleteTextView;

import cybersky.folios.R;
import cybersky.folios.adapters.Tags;

/**
 * Created by Rajat on 9/25/2015.
 */
public class TagsCompletionView extends TokenCompleteTextView<Tags> {
    public TagsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View getViewForObject(Tags person) {

        LayoutInflater l = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout)l.inflate(R.layout.tag_token, (ViewGroup)TagsCompletionView.this.getParent(), false);
        ((TextView)view.findViewById(R.id.name)).setText(person.getName());

        return view;
    }

    @Override
    protected Tags defaultObject(String completionText) {
        //Stupid simple example of guessing if we have an email or not
        int index = completionText.indexOf(',');
        if (index == -1) {
            return new Tags(completionText);
        } else {
            return new Tags(completionText.substring(0, index));
        }
    }
}